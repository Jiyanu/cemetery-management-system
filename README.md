# Saints Peter & Paul Cemetery Management System

### Client: Kim Hunt

### Project Title

Saints Peter & Paul Cemetery Management System is a user-friendly business-management software that will run remotely on the web.It 
will be database driven which will contain features to process and manage records, 
provide lot mapping, analyze accounting activities and administer cemetary
operations. It is directed to enhances the user experience by increasing overall 
efficiency and effectiveness to manage the business of the cemetry management system. 

### Motivation 

The traditional way of storing records in a paper-based system lacks few good things like scalability, tracability, version-control system. It is time-consuming and error-prone. Searching, Editing and Saving the records of interred people and lot owner would be easier with a web-based management system. Fetching records and generating reports of the lot owners would not only understand customers and their needs, but also give a scope to provide a better customer-service.

### Website - Cemetry Management System

>Web application is hosted on the server and is accessible at: https://csvm16.cs.bgsu.edu/


### Technologies Used for this project

    React - Frontend
    Django - Backend
    PostGres - Database
    Docker Engine
    Visual Studio Code, Vim - Editors

### Team Members

    Prabesh
    Anusha
    Gage Thompson
    Nahom 


### Credits 
