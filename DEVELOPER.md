# Cemetery Management System

Cemetry management system is a system to manage and handle lot owners and interred people information. Information is related to the details of the lot owner, interred person, number of potential interred people for a single lot owner. System is able to handle different search options for lot owner and interred. The system is secured using login session management system. 

# Getting Started

### Installation Requirement


* [Text Editor of your choice](https://atom.io)
* [Command line interface of choice](https://hyper.is)
* [Docker engine depending on enviroment](https://docs.docker.com/engine/install)


### Clone the repo

    $ git clone https://gitlab.com/cs4540-spring-2019/cs4540-spring-2020/cs4540_sp2020_g02/

### How do you use docker with an IDE to make changes?

    IDEs such as NetBeans, Eclipse, IntelliJ and  Visual Studio have support for Docker through plugins or add ons.
    Also possible to develop docker projects through the command line API. 

### To run

    $ docker-compose build
    $ docker-compose up



### Log in Details

    Admin can register to the application and login with the credentials in frontend. 
    Log in details for backend : 
    Username: admin
    Password: admin



### Various Testing techniques we apply to this project:

    * Unit Testing
    * Integration Testing 
    * Functional Testing 
    * Regression Testing 

### Cemetary management system allows admin(Client) to:

    Add a lot owner
    Add Interred Info
    Search Blocks and information of the lot owner and the interred person
    Generate Reports

### Cemetary management system allows workers to:

    Search for the interred person block number


### Implemented Functionalities 

    Sign in to the application on the server and view application
    Register and log into the system 
    View interred and lot owner lists in various tabs from the dashboard page
    Search for Lot Owners using ID, User name
    Save the Lot Owner details to the database from Lot Owner Page with a sequence ID
    Search for Interred using ID, interred name 
    Add interred records from Lot Owner page
    Add tombstone images specific to each interred.
    View the relationship between the lot owner and interred
    Able to update lot owner and interred information


###  Django's functionality for our project includes:

Model Structure

- Project data requierment so far includes Lot Owner Model, Interred Model, and Tombstone Model
- Enable us to build custom information about our data that represents a database field
- Django gives an automatically-generated database-access API

Communication with database

- Django supports many third party databases. In this case working with Postgres
- CRUD operation can be easily connected with the axios library to the frontend
- Projects currently uses one-to-many database relationships 
- Django REST framework builds our projects web apis
- Grants the ability to perform manipulation from the backend with a great user interface

Authentication
   
- Handles different user accounts, groups, permissions and cookie-based user sessions
- Grants custom permissions for model alteration that can be checked through Django’s authorization system
- Flexiable to create a custom user model so that an email address can be used as the primary user identifier instead of a username for authentication.


Authorization
    
- Allows or deny user access for particular feature/url/action 
- Using the admin interface one can easily manage group, users and assign them permissions.
- URL paths are protected and unprotected routes based on each user


### Summary of the Project 
```
1. Lot Owner Page
    - Lot ID : starts from #12,000
    - Lot Owner Fields: Firstname, Lastname, Address, Number, Date Issued, Cost
    - No. of Lots purchased
    - Interred person:(click to add, a pop-up window opens to enter First Name and Last Name of the dead person)
        - Person 1 ***
        - Person 2 ***
        (Click on either of these people to go to Interred Info Page)
        (Star *** apears next to the person's name when a comment is updated on the Interred Info Page)

2. Interred Info Page
    - Unique ID: starts from #50,000
    - First Name, Last Name (Populate from lot owner page)
        - Click on the Name to go to Personal File Page
    - A picture icon (Redirects to a Picture info page)
        - Upload picture of marker/tombstone
        - Picture or drawing of marker
        - Approval date
        - Received dare
        - Marker dealer
    - Section of cemetery: (dropdown)
        - BCE, BCW, NCA, ......
    - Property Style: (dropdown)
        - Burial lot / Mausoleum
    - Date of death (pop-up calendar)
    - Buried date
    - Place of death
    - Mass time (dropdown)
    - Age
    - Gender
    - Name of church
    - Name of funeral home
    - Veteran (yes/no)
    - Marital Status
    - Spouse's Name
    - Notes section at the bottom ( More like individual comments)
        - Created By
        - Timestamp

3. A dashboard page (Has the following search blocks and return a lot owner and interred person's info)
    - Owners
    - Inteded or interred
    - Interment date
    - Property style
    - Burial style
    - Date of death
    - Gender
    - Mass Time
    - Cemetery Section
    - Lot

4. Admin and Workers Login Pages
```


