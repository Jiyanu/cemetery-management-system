# Testing

Total Test Cases from Sprint 1,2,3,4,5,6,7,8,9 - 101

| Total Test Cases | 101 |
| :------------- | :----------: |
|PASS |  101 |
|FAIL |  0 |

## Sprint 10

```
docker-compose exec backend bash -c  'python manage.py test interred_info'
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
.................12001
6
...
----------------------------------------------------------------------
Ran 20 tests in 0.762s

OK
Destroying test database for alias 'default'...
```
```
docker-compose exec backend bash -c 'python manage.py test lot_owner'
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
...................<class 'lot_owner.models.LotOwner'>
.
----------------------------------------------------------------------
Ran 20 tests in 0.135s

OK
Destroying test database for alias 'default'...
```

### Previous Failed Test Cases
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 92 | Tombstone PUT request is working from frontend |  Verify that PUT request is working from frontend | PASS |

### Interred
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 95 | CSV Export is working for the GET fields |  Verify that CSV export is working and all columns are exported | PASS |
| 96 | CSV Export is working for filtered data|  Verify that CSV export is working when search filter is applied | PASS |

### Add Lot Owner
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 97 | Adding new lot owner |  Verify that new lot owner can be added without nested interreds | PASS |
| 98 | Phone number|  Verify that phone number field accpets big integer | PASS |

### Add Interred
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 99 | Add New Interred Info |  Verify that POST request is working for a new interred | PASS |
| 100 | Interred Detail |  Verify that interred details are being pulled correctly in fields| PASS |
| 101 | PUT request from Interred Detail |  Verify that interred info is being updated correctly| PASS |



## Sprint 9

docker-compose exec backend bash -c  'python manage.py test lot_owner'
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
......
----------------------------------------------------------------------
Ran 6 tests in 0.065s

OK
Destroying test database for alias 'default'...

docker-compose exec backend bash -c  'python manage.py test interred_info'
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
......
----------------------------------------------------------------------
Ran 6 tests in 0.328s

OK
Destroying test database for alias 'default'...


### Previous Failed Test Cases
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 25 | http://localhost:3000/interred_list/afa |  Able to view 404 Error page while checking invalid URL| PASS |
| 68 | Gitlab Pipelines/Jobs |  Gitlab runner failed probably because of react update | PASS |

### LotOwner
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 88 | CSV Export is working for the GET fields |  Verify that CSV export is working and all columns are exported | PASS |
| 89 | CSV Export is working for filtered data|  Verify that CSV export is working when search filter is applied | PASS |
| 90 | Interred Column in Lot owner table|  Verify that Interred Firstname and Lastname are pulled for each lot owner | PASS |

### Tombstone
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 91 | Tombstone detail view is working |  Verify that Tombstone url is working for the url| PASS |
| 92 | Tombstone PUT request is working from frontend |  Verify that PUT request is working from frontend | FAIL |

### Interred Details Page
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 93 | Interred detail page|  Verify that the data is matching for lot owner ID | PASS |
| 94 | Update the Interred Details |  Verify that PUT request is working | PASS |


## Sprint 8

### Lot Owner List Page
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 81 | Lot owner list page|  Verify that the View Details Link is working | PASS |

### Interred List Page
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 82 | Interred list page|  Verify that the View Details Link is working | PASS |

### Lot Owner Details Page
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 83 | Lot owner detail page|  Verify that the data is matching for lot owner ID | PASS |
| 84 | Update the Lot Owner Details |  Verify that PUT request is working | PASS |

### Picture Page
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 85 | Add Tombstone Model for Interred |  Verify that the model is working from backend | PASS |
| 86 | Upload Tombstone Image for Interred |  Verify that image is being saved | PASS |

### Loading
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 87 | Home Page |  Verify that memory leak warning is gone | PASS |


## Sprint 7

### Interred Page
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 72 | Search Functionality in Interred Page|  Verify that search works on each field | PASS |
| 73 | Search Clear Filters in Interred Page|  Verify that the reset filter buttons are working | PASS |

### Routes and Navigation
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 74 | Sitewide routes and navigation behavior |  Verify that the links are working from Navbar | PASS |


### Lot Owner Page
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 75 | Dynamic fields for potential interred field |  Verify to add and delete dynamic fields | PASS |
| 76 | Dynamic fields for potential interred field |  Verify that POST Request works | PASS |
| 77 | Dynamic fields for potential interred field |  Verify that POST Request works for multiple interred fields | PASS |
| 78 | Dynamic fields for potential interred field |  Verify that stored data are being populated properly in Lot owner and Interred page| PASS |

### Picture Page
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 79 | Picture Page Route  |  Verify that the link navigates to desired address  | PASS |
| 80 | Image Display & Upload  |  Verify that desired images are displayed on screen before upload | PASS |

## Sprint 6

### Gitlab Runner (CI)
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 68 | Gitlab Pipelines/Jobs |  Gitlab runner failed probably because of react update | FAIL |

### Loading
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 69 | Loading animation to manage async process|  Verify that loading works pulling the serverside data | PASS |

### Serverside Session Management
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 70 | Login Session in react and django |  Verify that login works in deployment server | PASS |

### Lot Owner Page
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 71 | Dynamic fields for potential interred field |  Verify to add and delete dynamic fields | PASS |

### Few tests to test source code: 

    docker-compose exec backend bash -c  'python3 manage.py test lot_owner.tests'
    Creating test database for alias 'default'...
    System check identified no issues (0 silenced).
    ....
    ----------------------------------------------------------------------
    Ran 4 tests in 0.033s

    OK
    Destroying test database for alias 'default'...


    docker-compose exec backend bash -c  'python3 manage.py test interred_info.tests'
    Creating test database for alias 'default'...
    System check identified no issues (0 silenced).
    .....
    ----------------------------------------------------------------------
    Ran 5 tests in 0.265s

    OK
    Destroying test database for alias 'default'...




## Sprint 5

### Gitlab Runner (CI)
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 62 | Gitlab Pipelines/Jobs |  Gitlab runner is running successfully after each merge | PASS |

### Lot Owner Page
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 63 | Search Functionality in Lot Owner|  Verify that search works on each field | PASS |
| 64 | Search Clear Filters in Lot Owner|  Verify that the reset filter buttons are working | PASS |
| 65 | POST Requests in Lot Owner|  Verify that the POST request is working | PASS |
| 66 | GET Requests in Lot Owner after POST|  Verify that the POST request is working along with new GET | PASS |

### Authentication
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 67 | Unauthenticated users cannot acces Lot Owner and Interred Page |  Verify that only logged in users can access | PASS |

## Sprint 4

### Gitlab Runner (CI)
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 44 | Gitlab Pipelines/Jobs |  Able to see if gitlab runner is running | PASS |
| 45 | Gitlab Runner Frontend Tests |  Verify in the jobs page that all tests have passed | PASS |
| 46 | Gitlab Runner Branch |  Verify that gitlab CI is triggred only when develop branch is updated | PASS |

### REST request backend and frontend 
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 47 | http://localhost:8000/interred_info/ | Able to view list of interred, Rest request (GET) working| 	PASS |
| 48 | http://localhost:3000/interred_list | Able to see existing interred info in the frontend , GET request for interred_info API from frontend|	PASS |
| 49 | http://localhost:8000/interred_info/50000 | Able to view existing interred info of Interred ID 50000 in the frontend. GET request for interred_info API from backend|	PASS |
| 50 | http://localhost:3000/interred_list/50000 | Able to view interred details of Interred ID 50000 in the frontend. GET request for interred_info API from frontend|	PASS |
| 51 | localhost:8000/interred_info/50000 | Able to view list of interred details of Interred ID 50000 in the frontend| PASS |

### Relations in the Database 
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 52 | localhost:3000/admin, Fill interred entry in backend | Able to view lot owner column with existing lot owners in the Interred_info model | PASS |
| 53 | localhost:3000/admin, Fill interred entry in backend | Able to view created by column with existing users  in the Interred_info model | PASS | 

### Lot Owner Pop up
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 54 | http://localhost:3000/lot_owner  | Able to view 'plus button' located at the left corner of the lot owner page, it creates a pop up windows to add Lot owner information |	PASS |
| 55 | Input Fields |	Able to recive information and later do operation on it | PASS |
| 56 | Return Button |	Able to close the pop up and return to the lot owner page | PASS |
| 57 | Reset Button |	Should be able to reset the input fields in the form | FAIL |

### New Login Page
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 58 | http://localhost:3000/ | Able to view login page with new background image | PASS |
| 59 | http://localhost:3000/ | Able to use same old login/register functionality | PASS |
| 60 | http://localhost:3000/ | Able to view error message if login is unsuccessful | PASS |

### Miscellaneous Fixes
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 61 | Navbar  | Able to hover over all navbar options and receive a pointer-cursor | PASS |

## Sprint 3 


### NoURL 404 Error Page
|Test Number    | Test  | Functionality | Result    |
| :------------- | :----------: | -----------: |-----------: |
| 24 | http://localhost:3000/dsfdsf |  Able to view 404 Error page while checking invalid URL | PASS |
| 25 | http://localhost:3000/lot_owner/afa |  Able to view 404 Error page while checking invalid URL| PASS |
| 25 | http://localhost:3000/interred_list/afa |  Able to view 404 Error page while checking invalid URL| FAIL |

### Lot Owner Pop up
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 26 |	http://localhost:3000/lot_owner  | Able to view 'plus button' located at the left corner of the lot owner page, it creates a pop up windows to add Lot owner information |	PASS |
| 27 | Input Fields |	Able to recive information and later do operation on it | PASS |
| 28 | Close Button |	Able to close the pop up and return to the lot owner page | PASS |

### Interred Info Backend
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 29 |	http://localhost:8000/admin/ |	View Admin panel |	PASS |
| 30 | Log into admin page using Username = admin , password = admin |	View Site for the admin with lot_owner,interred Model	| PASS |
| 31 | Create a Interred object	| Able to enter all fields and create a interred user |	PASS |
| 32 | Verify the django models and fields from django admin panel. |	Able to verify the django models and fields from django admin panel. | 	PASS |
| 33 | interred_info_id starting from 50000	| Able to see interred_info_id starting with 50000 in the url after creating lot owner |	PASS |
| 34 | interred_id incrementing after adding second interred |	Able to see interred_id starting with 50001 in the url after creating second interred person |PASS |
| 35 | Check if the Custom SQL migration was working for everyone |	Do this manually for the interred users |	PASS  |


### Login Functionality
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 36 |	Register new user |	Should add a new user to database |	PASS |
| 37 | Login functionality|	Successfull login visible via react-redux | PASS |
| 38 | Pages visibility| Only logged in users will be able to see the new pages |	PASS |
| 39 | Login/Registration | Throws error when the authentication fails |	PASS |
| 40 | Login/Registration | Redirects to the login page on fail login |	PASS |


### Rest Request
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 41 | Rest request (GET) working |	localhost:8000/lot_owner is working| 	PASS |
| 42 | GET request for lot_owner API from frontend| Able to see existing lot owner info in the frontend |	PASS |
| 43 | New addition of lot_owner |  On page refresh, a new entry should be visible in frontend |	PASS |



### Sprint 2

### Environment SetUp
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 1	| docker-compose build |	Builds the images, does not start the containers |	PASS |
| 2	| docker-compose up	| Builds the images if the images do not exist and starts the containers |	PASS |
| 3	| http://localhost:8000/ |	Able to see the message in page localhost:8000 "django ...The install worked successfully! Congratulations!" |	PASS |
| 4	| http://localhost:3000/ |	Able to view frontend page |	PASS |

### Homepage
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 5 |	View links on Navigation page | Able to view links that is Home, Login, Interred, Register on the home page |	PASS |
| 6 | Click on Login Page |	Able to view login page with username, password and submit fields | PASS |
| 7 | Click on Register Page |	Able to view "This is register page" | PASS |
| 8 | Click on Interred Page | Able to view form with various fields |	PASS |

### Interred Information Page
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 9 | Click on Section of Cemetry field |	View 13 options for this select option |	PASS |
| 10| Click on Property Style field |	View 2 options 	| PASS |
| 11 |	Click on Date of Death field 	 | View a calender and able to select date |	PASS |
| 12 |	Write date of death manually in DOD field |	Able to write date of death manually |	PASS |
| 13 |	Click on Buried Date field 	| View a calender and able to select date	| PASS |
| 14 |	Write date of death manually in DOD field |	Able to write buried date manually |	PASS |
| 15 | Select Mass Time Field |	Able to view 17 different timings from 8:00 to 3:30	|PASS |
| 16 | Select Marital Status |	Able to view 5 options |	PASS |


### Lot-Owner Backend		
|Test Number	| Test	| Functionality	| Result |
| :------------- | :----------: | -----------: |-----------: |
| 17 |	http://localhost:8000/admin/ |	View Admin panel |	PASS |
| 18 | Log into admin page using Username = admin , password = admin |	View Site for the admin and Lot_Owner Model	| PASS |
| 19 | Create a lot owner	| Able to enter all fields and create a lot owner |	PASS |
| 20 |Verify the django models and fields from django admin panel. |	Able to verify the django models and fields from django admin panel. | 	PASS |
| 21 | lot_owner_id starting from 12000	| Able to see lot_owner_id starting with 12000 in the url after creating lot owner |	PASS |
| 22 | lot_owner_id incrementing after adding second owner |	Able to see lot_owner_id starting with 12001 in the url after creating second lot owner |PASS |
| 23 | Check if the Custom SQL migration was working for everyone |	Able to view 12000 id in URL |	PASS  |

