from django.test import TestCase

# Create your tests here.

from django.test import TestCase

from .models import LotOwner


class LotOwnerTest(TestCase):
    @classmethod
    def setUpTestData(self, FirstName='Big', LastName='Bob', AptNo='2', Zip='3443',
                      PhoneNumber='7287524123', DateIssued='2020-03-19', TotalNoOfLots='3', Cost='3333.00'):
        # Set up non-modified objects used by all test methods
        return LotOwner.objects.create(FirstName=FirstName, LastName=LastName, AptNo=AptNo, Zip=Zip,
                                       PhoneNumber=PhoneNumber, DateIssued=DateIssued, TotalNoOfLots=TotalNoOfLots, Cost=Cost)

    def test_whatever_creation(self):
        w = self.setUpTestData()
        print(LotOwner)
        self.assertTrue(isinstance(w, LotOwner))

    def test_first_name_label(self):
        LotOwner.objects.create(FirstName='lot', LastName='ownername', AptNo='2', Zip='3443',
                                PhoneNumber='7524788888', DateIssued='2020-03-19', TotalNoOfLots='3', Cost='32423.00'
                                )
        lotowner = LotOwner.objects.get(id=12000)
        field_label = lotowner._meta.get_field(
            'FirstName').verbose_name
        self.assertEquals(field_label, 'First Name')

    def test_TotalNoOfLots_label(self):
        lotowner = LotOwner.objects.get(id=12000)
        field_label = lotowner._meta.get_field('TotalNoOfLots').verbose_name
        self.assertEquals(field_label, 'Number of lots')

    def test_first_name_max_length(self):
        lotowner = LotOwner.objects.get(id=12000)
        max_length = lotowner._meta.get_field('FirstName').max_length
        self.assertEquals(max_length, 200)

    def test_AptNo_label(self):
        lotowner = LotOwner.objects.get(id=12000)
        label = lotowner._meta.get_field('AptNo').verbose_name
        self.assertEquals(label, 'Apartment Number')

    def test_Zip_label(self):
        lotowner = LotOwner.objects.get(id=12000)
        label = lotowner._meta.get_field('Zip').verbose_name
        self.assertEquals(label, 'Postal Code')

    def test_PhoneNumber_label(self):
        lotowner = LotOwner.objects.get(id=12000)
        label = lotowner._meta.get_field('PhoneNumber').help_text
        self.assertEquals(label, 'Contact phone number')

    def test_object_name_is_last_name_comma_first_name(self):
        lotowner = LotOwner.objects.get(id=12000)
        expected_object_name = f'{lotowner.LastName}, {lotowner.FirstName}'
        self.assertEquals(expected_object_name, str(lotowner))

    def test_post_list_lot_owner(self):
        response = self.client.get('/lot_owner/')
        self.assertEqual(response.status_code, 200)

    def test_lo_details(self):
        # Issue a GET request.
        response = self.client.get('/lot_owner/12000/')

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

    def test_home_page_notcontain_code(self):
        response = self.client.get('/lot_owner/')
        self.assertNotContains(
            response, 'Hi there! I should not be on the page.')

    def test_detailpage_code(self):
        response = self.client.get('/lot_owner/12000/')
        self.assertNotContains(
            response, 'Hi there! I should not be on the page.')

    def test_tomburl(self):
        response = self.client.get('/tombstone/')
        self.assertEqual(response.status_code, 200)

    def test_tombstone_notcontain_code(self):
        response = self.client.get('/tombstone/')
        self.assertNotContains(
            response, 'Hi there! I should not be on the page.')

    def test_users_notcontain_code(self):
        response = self.client.get('/users/')
        self.assertNotContains(
            response, 'Hi there! I should not be on the page.')

    def test_users_code(self):
        response = self.client.get('/users/')
        self.assertEqual(response.status_code, 200)

    def test_lo(self):
        lotowner = LotOwner()
        lotowner.FirstName = 'john'
        lotowner.LastName = 'sjdfh'
        lotowner.PhoneNumber = '9234782378'
        lotowner.AptNo = '9'
        lotowner.Zip = '22'
        lotowner.TotalNoOfLots = '2'
        lotowner.Cost = '200.00'
        lotowner.save()
        self.assertEqual(lotowner.FirstName, 'john')

    def test_ln(self):
        lotowner = LotOwner()
        lotowner.FirstName = 'john'
        lotowner.LastName = 'sjdfh'
        lotowner.PhoneNumber = '9234782378'
        lotowner.AptNo = '9'
        lotowner.Zip = '22'
        lotowner.TotalNoOfLots = '2'
        lotowner.Cost = '200.00'
        lotowner.save()
        self.assertEqual(lotowner.LastName, 'sjdfh')

    def test_pn(self):
        lotowner = LotOwner()
        lotowner.FirstName = 'john'
        lotowner.LastName = 'sjdfh'
        lotowner.PhoneNumber = '9234782378'
        lotowner.AptNo = '9'
        lotowner.Zip = '22'
        lotowner.TotalNoOfLots = '2'
        lotowner.Cost = '200.00'
        lotowner.save()
        self.assertEqual(lotowner.PhoneNumber, '9234782378')

    def test_cost(self):
        lotowner = LotOwner()
        lotowner.FirstName = 'john'
        lotowner.LastName = 'sjdfh'
        lotowner.PhoneNumber = '9234782378'
        lotowner.AptNo = '9'
        lotowner.Zip = '22'
        lotowner.TotalNoOfLots = '2'
        lotowner.Cost = '200.00'
        lotowner.save()
        self.assertEqual(lotowner.Cost, '200.00')

    def test_PhnNo(self):
        lotowner = LotOwner()
        lotowner.FirstName = 'john'
        lotowner.LastName = 'sjdfh'
        lotowner.PhoneNumber = '9999999923'
        lotowner.AptNo = '9'
        lotowner.Zip = '22'
        lotowner.TotalNoOfLots = '2'
        lotowner.Cost = '200.00'
        lotowner.save()
        self.assertEqual(lotowner.FirstName, 'john')
