from rest_framework import viewsets
from lot_owner.models import LotOwner
from .serializers import LotOwnerSerializer


class LotOwnerViewSet(viewsets.ModelViewSet):
    serializer_class = LotOwnerSerializer
    queryset = LotOwner.objects.all()

    def get_serializer(self, *args, **kwargs):
        data = kwargs.get("data")
        if data and isinstance(data, list):
            kwargs["many"] = True
        return super(LotOwnerViewSet, self).get_serializer(*args, **kwargs)
