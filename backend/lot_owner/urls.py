
from django.urls import path

from lot_owner.views import LotOwnerViewSet

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'', LotOwnerViewSet, basename='user')
urlpatterns = router.urls
