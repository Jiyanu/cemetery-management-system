from django.db import models
from phone_field import PhoneField

# Create your models here.


class LotOwner(models.Model):
    id = models.AutoField(primary_key=True)
    FirstName = models.CharField(max_length=200, verbose_name="First Name")
    LastName = models.CharField(max_length=200, verbose_name="Last Name")
    StreetAddress = models.TextField()
    AptNo = models.IntegerField(verbose_name="Apartment Number")
    City = models.TextField()
    State = models.TextField()
    Country = models.TextField()
    Zip = models.IntegerField(verbose_name="Postal Code")
    PhoneNumber = models.BigIntegerField(blank=True, help_text='Contact phone number')
    DateIssued = models.DateField(blank=True, null=True)
    TotalNoOfLots = models.IntegerField(verbose_name="Number of lots")
    Cost = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
        return f'{self.LastName}, {self.FirstName}'
