from django.apps import AppConfig


class LotOwnerConfig(AppConfig):
    name = 'lot_owner'
