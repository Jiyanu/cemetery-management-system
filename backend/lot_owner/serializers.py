from rest_framework import serializers

from lot_owner.models import LotOwner
from interred_info.serializers import InterredSerializer
from interred_info.models import Interred


class LotOwnerSerializer(serializers.ModelSerializer):

    interreds = InterredSerializer(source='interred_set', many=True)

    class Meta:
        model = LotOwner
        fields = ('__all__')

    def create(self, validated_data):
        interred_data_set = validated_data.pop('interred_set')
        Lot_owner = LotOwner.objects.create(**validated_data)
        for interred_data in interred_data_set:
            Interred.objects.create(**interred_data, LotOwner=Lot_owner)
        return Lot_owner

    def update(self, instance, validated_data):
        interred_data_set = validated_data.pop('interred_set')
        inter_instances = (instance.interred_set).all()
        inter_instances = list(inter_instances)
        instance.FirstName = validated_data.get(
            'FirstName', instance.FirstName)
        instance.LastName = validated_data.get('LastName', instance.LastName)
        instance.StreetAddress = validated_data.get(
            'StreetAddress', instance.StreetAddress)
        instance.AptNo = validated_data.get('AptNo', instance.AptNo)
        instance.City = validated_data.get('City', instance.City)
        instance.State = validated_data.get('State', instance.State)
        instance.Country = validated_data.get('Country', instance.Country)
        instance.Zip = validated_data.get('Zip', instance.Zip)
        instance.PhoneNumber = validated_data.get(
            'PhoneNumber', instance.PhoneNumber)
        instance.DateIssued = validated_data.get(
            'DateIssued', instance.DateIssued)
        instance.Cost = validated_data.get('Cost', instance.Cost)
        instance.TotalNoOfLots = validated_data.get(
            'TotalNoOfLots', instance.TotalNoOfLots)
        instance.save()

        for interred_data in interred_data_set:
            inter = inter_instances.pop(0)
            inter.inter_fname = interred_data.get(
                'inter_fname', inter.inter_fname)
            inter.inter_lname = interred_data.get(
                'inter_lname', inter.inter_lname)
            inter.save()
        return instance
