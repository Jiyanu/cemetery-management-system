from django.urls import path
#from .views import current_user, UserList
#from users.views import UserList

from .views import UserListView

from rest_framework.routers import DefaultRouter

"""
urlpatterns = [
    path('current_user/', current_user),
    path('users/', UserList.as_view())
]
"""

router = DefaultRouter()
router.register(r'', UserListView, basename='username')
urlpatterns = router.urls

