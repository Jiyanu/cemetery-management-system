from django.contrib.auth import get_user_model
from .models import LotOwner
from .models import Interred
from django.test import TestCase


class InterredTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        lotowner = LotOwner.objects.create(FirstName='lot', LastName='ownername', AptNo='2', Zip='3443',
                                           PhoneNumber='7524788888', DateIssued='2020-03-19', TotalNoOfLots='3', Cost='32423.00'
                                           )
        User = get_user_model()
        user = User.objects.create_user(
            username='sdfdf', email='normal@user.com', password='foosdfsdfs')
        Interred.objects.create(
            LotOwner=lotowner, inter_fname='Joe', inter_lname='sfsdf', cemetry_section='OS Memory Garden', grave_number='242', lot_number='2433443', property_style='Mausoleum', date_of_death='2020-03-11', age='33', date_of_birth='1980-03-09', date_created='2020-03-31', notes='sdfs', spouse_name='sdf', church_name='fsdfs', birth_place='sfsfw', gender='Male', place_of_death='jhadjkfh', interment_date='2020-03-11', mass_time='8:00', veteran_status='Yes', marital_status='Married', created_by=user)

    def test_first_name_label(self):
        interred = Interred.objects.get(id=50000)
        field_label = interred._meta.get_field('inter_fname').verbose_name
        self.assertEquals(field_label, 'Interred First Name')

    def test_place_of_death_label(self):
        interred = Interred.objects.get(id=50000)
        field_label = interred._meta.get_field('place_of_death').verbose_name
        self.assertEquals(field_label, 'Place of Death')

    def test_graveno_label(self):
        interred = Interred.objects.get(id=50000)
        field_label = interred._meta.get_field('grave_number').verbose_name
        self.assertEquals(field_label, 'Grave Number')

    def test_lot_number_label(self):
        interred = Interred.objects.get(id=50000)
        field_label = interred._meta.get_field('lot_number').verbose_name
        self.assertEquals(field_label, 'Lot Number')

    def test_interment_date_label(self):
        interred = Interred.objects.get(id=50000)
        field_label = interred._meta.get_field('interment_date').verbose_name
        self.assertEquals(field_label, 'Interment Date')

    def test_first_name_max_length(self):
        interred = Interred.objects.get(id=50000)
        max_length = interred._meta.get_field('inter_fname').max_length
        self.assertEquals(max_length, 100)

    def test_birth_place_max_length(self):
        interred = Interred.objects.get(id=50000)
        max_length = interred._meta.get_field('birth_place').max_length
        self.assertEquals(max_length, 50)

    def test_church_name_max_length(self):
        interred = Interred.objects.get(id=50000)
        max_length = interred._meta.get_field('church_name').max_length
        self.assertEquals(max_length, 200)

    def test_funeral_home_name_max_length(self):
        interred = Interred.objects.get(id=50000)
        max_length = interred._meta.get_field('funeral_home_name').max_length
        self.assertEquals(max_length, 200)

    def test_veteran_status_max_length(self):
        interred = Interred.objects.get(id=50000)
        max_length = interred._meta.get_field('veteran_status').max_length
        self.assertEquals(max_length, 10)

    def test_object_name_is_last_name_comma_first_name(self):
        interred = Interred.objects.get(id=50000)
        expected_object_name = f'{interred.inter_lname}, {interred.inter_fname}'
        self.assertEquals(expected_object_name, str(interred))

    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(username='hellouser',
                                        email='normali@user.com', password='foofsdfssf')
        self.assertEqual(user.email, 'normali@user.com')

    def test_create_user_active(self):
        User = get_user_model()
        user = User.objects.create_user(username='user1',
                                        email='normali@user.com', password='foofsdfssf')
        self.assertTrue(user.is_active)

    def test_create_user_staff(self):
        User = get_user_model()
        user = User.objects.create_user(username='user2',
                                        email='normali@user.com', password='foofsdfssf')
        self.assertFalse(user.is_staff)

    def test_create_user_superuser(self):
        User = get_user_model()
        user = User.objects.create_user(username='user3',
                                        email='normali@user.com', password='foofsdfssf')
        self.assertFalse(user.is_superuser)

    def test_post_list_interred_view(self):
        response = self.client.get('/interred_info/')
        self.assertEqual(response.status_code, 200)

    def test_details(self):
        # Issue a GET request.
        response = self.client.get('/interred_info/50000/')

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

    def test_interdetailpage_code(self):
        response = self.client.get('/interred_info/50000/')
        self.assertNotContains(
            response, 'Hi there! I should not be on the page.')

    def test_interpage_code(self):
        response = self.client.get('/interred_info/')
        self.assertNotContains(
            response, 'Hi there! I should not be on the page.')

    def check_details(self):
        # Issue a GET request.
        response = self.client.get('/interred_info/')

        # Check that the response is 200 OK.
        self.assertEqual(len(response.context['interred_info']), 1)

    def test_post(self):
        lotowner = LotOwner.objects.create(FirstName='hello', LastName='ownername', AptNo='2', Zip='3443',
                                           PhoneNumber='7524788888', DateIssued='2020-03-19', TotalNoOfLots='3', Cost='32423.00'
                                           )
        lotowner_id = LotOwner.objects.get(FirstName='hello').id
        print(lotowner_id)
        User = get_user_model()
        user = User.objects.create(
            username='ndbfsdnf', email='normal@user.com', password='foosdfsdfs')
        user_id = User.objects.get(username='ndbfsdnf').id
        print(user_id)
        data = {
            "inter_fname": "new",
            "inter_lname": "kjklef",
            "cemetry_section": "NC A (New Cemetery A)",
            "grave_number": 3,
            "lot_number": 3,
            "property_style": "Burial Lot",
            "date_of_death": "2020-04-27",
            "interment_date": "2020-04-27",
            "place_of_death": "efdsf",
            "age": 232,
            "gender": "Male",
            "date_of_birth": "2020-04-27",
            "birth_place": "dsfdsf",
            "mass_time": "9:00",
            "church_name": "sdfsdf",
            "funeral_home_name": "sdfsf",
            "veteran_status": "Yes",
            "marital_status": "Married",
            "spouse_name": "sdfsd",
            "notes": "sdfsdf",
            "date_created": "2020-04-27",
            "created_by_id": user_id,
            "LotOwner_id": lotowner_id
        }
        response = self.client.post('/interred_info/', data)
    #    self.assertEqual(response.status_code, 200)
    #    self.assertTrue(response.status_code, 500)
