from django.db import models
from lot_owner.models import LotOwner
from django.conf import settings
from django.contrib.auth import get_user_model
from datetime import datetime
# Create your models here.


class Interred(models.Model):

    # username = models.CharField(max_length=200, verbose_name="Name")
    CEMETRY_SECTION_CHOICES = [
        ('BCE', 'BC E (baby cemetery east)'),
        ('BCW', 'BCW (baby cemetery west)'),
        ('NCA', 'NC A (new cemetery A)'),
        ('NCB', 'NC B (new cemetery B)'),
        ('NCC', 'NC C (new cemetery C)'),
        ('NCD', 'NC D (new cemetery D)'),
        ('NCE', 'NC E (new cemetery E)'),
        ('NCF', 'NC F (new cemetery F)'),
        ('NCG', 'NC G (new cemetery G)'),
        ('NCH', 'NC H (new cemetery H)'),
        ('NCI', 'NC I (new cemetery I)'),
        ('OC', 'OC (old cemetery)'),
        ('OS', 'OS (old section)'),
        ('OSM', 'OS Memory Garden')
    ]
    PROPERTY_STYLE_CHOICES = [
        ('Burial Lot', 'Burial Lot'), ('Mausoleum', 'Mausoleum')]
    GENDER_CHOICES = [('Male', 'Male'), ('Female', 'Female')]
    MASS_TIME_CHOICES = [
        ('8:00', '8:00'),
        ('8:30', '8:30'),
        ('9:00', '9:00'),
        ('9:30', '9:30'),
        ('10:00', '10:00'),
        ('10:30', '10:30'),
        ('11:00', '11:00'),
        ('11:30', '11:30'),
        ('12:00', '12:00'),
        ('12:30', '12:30'),
        ('1:00', '1:00'),
        ('1:30', '1:30'),
        ('2:00', '2:00'),
        ('2:30', '2:30'),
        ('3:00', '3:00'),
        ('3:30', '3:30')
    ]
    VETERAN_STATUS_CHOICES = [('Yes', 'Yes'), ('No', 'No')]
    MARITAL_STATUS_CHOICES = [
        ('Married', 'Married'), ('Single', 'Single'), ('Widow', 'Widow'), ('Divorced', 'Divorced'), ('Unknown', 'Unknown')]

    id = models.AutoField(primary_key=True)
    LotOwner = models.ForeignKey(
        LotOwner, on_delete=models.CASCADE, default='', null=True)
    inter_fname = models.CharField(
        max_length=100, verbose_name="Interred First Name", default='')
    inter_lname = models.CharField(
        max_length=100, verbose_name="Interred Last Name",  default='')
    cemetry_section = models.CharField(
        max_length=100, choices=CEMETRY_SECTION_CHOICES, null=True)
    grave_number = models.IntegerField(verbose_name="Grave Number", null=True)
    lot_number = models.IntegerField(verbose_name="Lot Number", null=True)
    property_style = models.CharField(
        blank=True, choices=PROPERTY_STYLE_CHOICES, max_length=10, null=True)
    # username = models.
    date_of_death = models.DateField(verbose_name="Date of Death", null=True)
    interment_date = models.DateField(verbose_name="Interment Date", null=True)
    place_of_death = models.CharField(
        max_length=100, verbose_name="Place of Death", null=True)
    age = models.IntegerField(verbose_name="Age", null=True)
    gender = models.CharField(
        max_length=10, verbose_name="Gender", choices=GENDER_CHOICES, null=True)
    date_of_birth = models.DateField(verbose_name='Date of Birth', null=True)
    birth_place = models.CharField(
        verbose_name='Birth Place', max_length=50, null=True)
    mass_time = models.CharField(
        max_length=100, choices=MASS_TIME_CHOICES, null=True)
    church_name = models.CharField(
        verbose_name='Church Name', max_length=200, null=True)
    funeral_home_name = models.CharField(
        verbose_name='Funeral Home Name', max_length=200, null=True)
    veteran_status = models.CharField(
        max_length=10, choices=VETERAN_STATUS_CHOICES, null=True)
    marital_status = models.CharField(
        max_length=50, choices=MARITAL_STATUS_CHOICES, null=True)
    spouse_name = models.CharField(
        verbose_name='Spouse Name', max_length=100, null=True)
    notes = models.TextField(verbose_name="Notes", max_length='500', null=True)

    #created_by = models.CharField(max_length=100, verbose_name='Created By')
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.NOT_PROVIDED, null=True)
    date_created = models.DateField(blank=True, null=True)

    def __str__(self):
        return f'{self.inter_lname}, {self.inter_fname}'
