from django.apps import AppConfig


class InterredConfig(AppConfig):
    name = 'interred_info'
