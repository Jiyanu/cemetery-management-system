from rest_framework import serializers

from users.serializers import UserSerializer
from interred_info.models import Interred

from lot_owner.models import LotOwner
from rest_framework.fields import empty


class InterredSerializer(serializers.ModelSerializer):

    created_by_name = UserSerializer(source='created_by', read_only=True)

    class Meta:
        model = Interred
        fields = ('__all__')

    def run_validation(self, data=empty):
        return data
