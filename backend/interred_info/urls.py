
from django.urls import path


from rest_framework.routers import DefaultRouter
from .views import InterredListView, InterredDetailView

router = DefaultRouter()
router.register(r'', InterredListView, basename='user')
urlpatterns = router.urls

# urlpatterns = [
#    path('', InterredListView.as_view()),
#    path('<pk>', InterredDetailView.as_view())
# ]
