from django.shortcuts import render

from rest_framework import viewsets
from interred_info.models import Interred
from .serializers import InterredSerializer

from rest_framework.generics import ListAPIView

from rest_framework.generics import RetrieveAPIView


class InterredListView(viewsets.ModelViewSet):
    serializer_class = InterredSerializer
    queryset = Interred.objects.all()


class InterredDetailView(RetrieveAPIView):
    serializer_class = InterredSerializer
    queryset = Interred.objects.all()
