#! /bin/bash
PYTHON3=${PYTHON3:-python3}


$PYTHON3 /app/backend/manage.py collectstatic --no-input &
$PYTHON3 /app/backend/manage.py migrate --no-input

/code/scripts/wait-for-it.sh ${DATABASE_HOST:-db}:${DATABASE_PORT:-5432}
uwsgi --ini /app/backend/deploy/configs/uwsgi.ini

