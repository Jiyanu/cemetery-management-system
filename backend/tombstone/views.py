from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from tombstone.models import Tombstone
from .serializers import TombstoneSerializer


class TombstoneViewSet(viewsets.ModelViewSet):
    serializer_class = TombstoneSerializer
    queryset = Tombstone.objects.all()
