
from django.urls import path

from tombstone.views import TombstoneViewSet

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'', TombstoneViewSet, basename='user')
urlpatterns = router.urls
