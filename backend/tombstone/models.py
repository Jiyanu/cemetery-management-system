from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User
from interred_info.models import Interred


def upload_path(instance, filename):
    return '/'.join(['images', str(instance.inter_id), filename])


class Tombstone(models.Model):

    inter_id = models.IntegerField(blank=True, null=True)
    approval_date = models.DateField(blank=True, null=True)
    received_date = models.DateField(blank=True, null=True)
    tombstone_drawing = models.ImageField(
        upload_to=upload_path, null=True, blank=True)
    tombstone_picture = models.ImageField(
        upload_to=upload_path, null=True, blank=True)
    marker_dealer_name = models.CharField(
        verbose_name='Marker dealer name', max_length=100, null=True)

    def __unicode__(self):
        return self.inter_id
