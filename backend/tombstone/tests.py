from .models import Tombstone
from django.test import TestCase
from django.urls import reverse

from rest_framework import test
from rest_framework.test import APIClient
from lot_owner.serializers import LotOwnerSerializer


class TombstoneTest(TestCase):

    def test_post_list_tombstone(self):
        response = self.client.get('/tombstone/')
        self.assertEqual(response.status_code, 200)
