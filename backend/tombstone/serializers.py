from rest_framework import serializers

from tombstone.models import Tombstone
from interred_info.serializers import InterredSerializer


class TombstoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tombstone
        fields = ('inter_id', 'approval_date', 'received_date',
                  'tombstone_drawing', 'tombstone_picture', 'marker_dealer_name')
