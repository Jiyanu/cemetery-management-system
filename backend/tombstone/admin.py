from django.contrib import admin

# Register your models here.

from .models import Tombstone

admin.site.register(Tombstone)
