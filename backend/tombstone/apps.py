from django.apps import AppConfig


class TombstoneConfig(AppConfig):
    name = 'tombstone'
