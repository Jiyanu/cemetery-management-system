import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import {
    createStore,
    compose,
    applyMiddleware,
} from 'redux';

import reducer from './store/reducers/auth';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, composeEnhancers(
    applyMiddleware(thunk)
));

test('renders learn react link', () => {
    const { getByText } = render(
        <Provider store={store}>
            <App/>
    </Provider>
    );
});
