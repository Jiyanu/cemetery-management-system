import React from 'react';
import { Route, Switch, } from "react-router-dom";

import Login from './views/Login/';
import Register from './views/Register/';
import LotOwnerList from './views/LotOwnerListView/';
import InterList from './views/InterredListView/';
import Interred from './views/AddInterred/index';
import ImageUpload from './views/AddInterred/picturePage';
import AddTombstone from './views/AddTombstone';
import AddLotOwner from './views/AddLotOwner';
import InterDetails from './views/InterredDetailView/';
import LotOwnerDetailView from './views/LotOwnerDetailView/';
import Loading from '../src/components/Loading';

export default class BaseRouter extends React.PureComponent {

    render() {
        return (
            <React.Fragment>
                <Switch>
                    {this.props.isAuthenticated ?
                        <React.Fragment>
                            <Switch>
                                <Route exact path="/add-interred/" component={Interred} />
                                <Route exact path="/lot-owner" component={LotOwnerList} {...this.props} />
                                <Route exact path="/lot-owner/:lotOwnerID" component={LotOwnerDetailView} {...this.props} />
                                <Route exact path="/interred/" component={InterList} {...this.props} />
                                <Route exact path="/interred/:interID" component={InterDetails} {...this.props} />
                                <Route exact path="/add-lot-owner" component={AddLotOwner} {...this.props} />
                                <Route exact path="/tombstone/:interID" component={ImageUpload} {...this.props} />
                                <Route exact path="/add-tombstone" component={AddTombstone} {...this.props} />
                                <Loading {...this.props} /> {/*Calls load then 404 if needed */}
                            </Switch>
                        </React.Fragment>
                        :
                        <React.Fragment>
                            <Switch>
                                <Route exact path="/" component={Login} />
                                <Route exact path="/register/" component={Register} />
                                <Loading {...this.props} />
                            </Switch>
                        </React.Fragment>
                    }
                </Switch>
            </React.Fragment>
        );
    }
}
