import React from 'react';
import './index.scss';


export default class NoURL extends React.PureComponent {
    render () {
        return (
            <div className="fof">
                <img className="four" src="/images/Grave.jpg" alt="Grave"/>
            </div>
        );
    }
}
