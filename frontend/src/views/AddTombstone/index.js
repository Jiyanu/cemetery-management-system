import React from 'react'
//import { useHistory } from "react-router-dom";
import { Form, Input, Button, Select, notification } from 'antd';
import './styles.scss';
import axios from 'axios';

const Option = Select.Option;

const layout = {

    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 10,
    },
};

const errorNotification = (placement) => {
    notification.open({
        message: `Oops!`,
        description:
            'Something went wrong. Please try again.', placement,
        onClick: () => {
            console.log('Notification Clicked!');

        },
    });
};

const failNotification = (placement) => {
    notification.open({
        message: `Submission Cancelled`,
        description:
            'User decided to halt submission.', placement,
        onClick: () => {
            console.log('Notification Clicked!');
        },
    });
};

const sentNotification = (placement) => {
    notification.open({
        message: `Success!`,
        description:
            'Details have been sent.', placement,
        onClick: () => {
            console.log('Notification Clicked!');
        },
    });
};


class AddTombstone extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            drawingfile: '',
            picturefile: '',
            tombstone: [],
            interred: [],
            search_data: [],
            imagePreviewUrldrawing: '',
            imagePreviewUrlpicture: '',
            new_inter: undefined,
            data: [],
            value: undefined,
            inter_options: [],
            approval_date: null,
            received_date: null,
            tombstone_drawing: null,
            tombstone_picture: null,
            marker_dealer_name: null,
            buttontype: "button",
        }
    };

    componentDidMount() {

        axios.get(`http://127.0.0.1:8000/interred_info/`)
            .then(res => {
                this.setState({
                    interred: res.data,
                });
                console.log("tomb-interred", res.data);
            })

    }


    _handleDrawingImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                drawingfile: file,
                imagePreviewUrldrawing: reader.result
            });
        }

        reader.readAsDataURL(file)
    }
    _handlePictureImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({

                picturefile: file,
                imagePreviewUrlpicture: reader.result
            });
        }

        reader.readAsDataURL(file)
    }


    confirmSubmit = () => {

        if (window.confirm("Are you sure you wish to continue?")) {
            this.setState({ buttontype: "Submit", });
            return true;
        }
        else {
            this.setState({ buttontype: "button", });
            failNotification('bottomLeft')
            return false;

        }
    }


    handleSubmit = (event) => {

        event.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                const interred_ID = parseInt(this.state.new_inter);
                const approval_date = event.target.elements.approval_date.value;
                const received_date = event.target.elements.received_date.value;

                const tombstone_drawing = event.target.elements.tombstone_drawing.files[0];
                const tombstone_picture = event.target.elements.tombstone_picture.files[0];

                const marker_dealer_name = event.target.elements.marker_dealer_name.value;
                console.log('iid', interred_ID);
                console.log('apdate', approval_date);
                console.log('revdate', received_date);
                //console.log('draw', tombstone_drawing.name);
                //console.log('pic', tombstone_picture.name);
                console.log('mark', marker_dealer_name);

                const uploadData = new FormData();
                uploadData.append('inter_id', interred_ID)
                uploadData.append('approval_date', approval_date)
                uploadData.append('received_date', received_date)
                if (tombstone_drawing) {
                    uploadData.append('tombstone_drawing', tombstone_drawing, tombstone_drawing.name)
                }
                if (tombstone_picture) {
                    uploadData.append('tombstone_picture', tombstone_picture, tombstone_picture.name)
                }
                uploadData.append('marker_dealer_name', marker_dealer_name)
                return axios.post('http://127.0.0.1:8000/tombstone/', uploadData, {
                    onUploadProgress: progressEvent => {
                        console.log('Upload Progress:' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + '%')
                    }
                })
                    .then(rsp => { console.log(rsp); sentNotification('bottomLeft') })
                    .catch(err => { console.log(err); errorNotification('bottomLeft') });

            }
            else {
                errorNotification('bottomLeft')
            };
        });
    }

    handleInterChange = (e) => {
        this.setState({
            new_inter: e,
        })
        console.log('selected', e);
    }

    getOptionList = (data) => {
        if (!data) {
            return [];
        }
        let options = [];
        data.forEach(item => {
            options.push(
                <Option value={item.id} key={item.id}>
                    {item.inter_fname}  {item.inter_lname}
                </Option>
            );
        });
        return options;
    }

    render() {
        const picture = this.state.tombstone;
        const inter = this.state.interred;

        const { getFieldDecorator } = this.props.form;
        getFieldDecorator('keys', { initialValue: [] });

        let { imagePreviewUrldrawing } = this.state;
        let { imagePreviewUrlpicture } = this.state;
        let $imagePreviewdrawing, $imagePreviewpicture = null;
        if (imagePreviewUrldrawing) {
            $imagePreviewdrawing = (<img src={imagePreviewUrldrawing} alt="imagePreviewUrldrawing" />);
        } else {
            $imagePreviewdrawing = (<div className="previewText">Please select an Image for Preview</div>);
        }
        if (imagePreviewUrlpicture) {
            $imagePreviewpicture = (<img src={imagePreviewUrlpicture} alt="imagePreviewUrlpicture" />);
        } else {
            $imagePreviewpicture = (<div className="previewText">Please select an Image for Preview</div>);
        }

        return (

            <div className="structure">

                <Form {...layout} name="nest-messages" onSubmit={(e) => this.handleSubmit(e)} >
                    <div className="top-header">
                        <h1 className="formTitle">Add Tombstone Form </h1>
                    </div>

                    <Form.Item label="Interred">
                        <Select
                            name='interred_ID'
                            onChange={this.handleInterChange}
                            placeholder="Select Interred" 
                        >
                            {this.getOptionList(inter)}
                        </Select>
                    </Form.Item>

                    <Form.Item label="Interred ID" className="Form_Field">
                        {getFieldDecorator('interred_ID', {
                            initialValue: this.state.new_inter || [],
                        })(
                            <Input type="number" name="interred_ID" ß readOnly placeholder="Select Interred Name from above" />
                        )}
                    </Form.Item>

                    <Form.Item label="Drawing of marker or tombstone" encType="multipart/form-data">
                        {getFieldDecorator('tombstone_drawing', {
                        })(
                            <React.Fragment>
                                <Input
                                    className="fileInput"
                                    type="file" name="tombstone_drawing"
                                    onChange={(e) => this._handleDrawingImageChange(e)}
                                />
                                <i>{picture.tombstone_drawing}</i>
                            </React.Fragment>
                        )}
                        <div className="imgPreview">{$imagePreviewdrawing}</div>
                    </Form.Item>

                    <Form.Item label="Picture of marker or tombstone" encType="multipart/form-data">
                        {getFieldDecorator('tombstone_picture', {
                        })(
                            <React.Fragment>
                                <Input
                                    className="fileInput"
                                    type="file"
                                    name="tombstone_picture"
                                    onChange={(e) => this._handlePictureImageChange(e)}
                                />
                                <i>{picture.tombstone_drawing}</i>
                                <div className="imgPreview">{$imagePreviewpicture}</div>
                            </React.Fragment>
                        )}

                    </Form.Item>

                    <Form.Item label="Approval Date" >
                        {getFieldDecorator('approval_date', {
                            initialValue: picture.approval_date || [],
                        })(
                            <Input type='date' name="approval_date" />)}
                    </Form.Item>

                    <Form.Item label="Recieved Date" >
                        {getFieldDecorator('received_date', {
                            initialValue: picture.received_date || [],
                        })(
                            <Input type='date' name="received_date" />)}
                    </Form.Item>

                    <Form.Item label="Name of Marker Dealer" >
                        {getFieldDecorator('marker_dealer_name', {
                            initialValue: picture.marker_dealer_name || [],
                        })(
                            <Input type='text' name="marker_dealer_name" />)}
                    </Form.Item>
                    <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                        <Button type="primary" className='submitButton' htmlType={this.state.buttontype} onClick={this.confirmSubmit}>
                            Submit
                    </Button>
                    </Form.Item>

                </Form>
            </div >

        )
    }
}

const WrappedImageForm = Form.create()(AddTombstone);
export default WrappedImageForm;
