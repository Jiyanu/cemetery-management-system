import React from "react";
import LoginForm from '../../components/LoginForm';

import './styles.css';

export default class Login extends React.PureComponent {
    render () {
        return (
            <div className="image-container">
                <LoginForm />
            </div>
        );
    }
}
