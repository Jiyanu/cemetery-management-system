import React from 'react';
import axios from 'axios';
//import Select from "react-select";
import { Select } from 'antd';
import { Form, Input, Button, notification } from 'antd';
import './styles.scss'
const { TextArea } = Input;
const Option = Select.Option;

const layout = {

    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 10,
    },
};

const errorNotification = (placement) => {
    notification.open({
        message: `Oops!`,
        description:
            'Something went wrong. Please try again.', placement,
        onClick: () => {
            console.log('Notification Clicked!');

        },
    });
};

const failNotification = (placement) => {
    notification.open({
        message: `Submission Halted`,
        description:
            'User decided to halt submission.', placement,
        onClick: () => {
            console.log('Notification Clicked!');
        },
    });
};

const sentNotification = (placement) => {
    notification.open({
        message: `Success`,
        description:
            'Details have been sent.', placement,
        onClick: () => {
            console.log('Notification Clicked!');
        },
    });
};


class InterDetails extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            inter: {},
            new_gender: "",
            new_mass_time: "",
            new_marital_status: "",
            new_veteran_status: "",
            new_cemetry_section: "",
            new_property_style: "",
            users: [],
            new_created_by: null,
            lot_owner: "",
            new_owner: "",
            buttontype: "button",
        }
    };

    componentDidMount() {
        const interID = this.props.match.params.interID;
        axios.get(`http://127.0.0.1:8000/interred_info/${interID}`)
            .then(res => {
                this.setState({
                    inter: res.data
                });
                console.log('interred-response', res.data)
            })

        axios.get(`http://127.0.0.1:8000/lot_owner/`)
            .then(res => {
                this.setState({
                    lot_owner: res.data,
                });
                console.log("interred-detail-lot-owner", res.data);
            })

        axios.get(`http://127.0.0.1:8000/users/`)
            .then(res => {
                this.setState({
                    users: res.data,
                });
                console.log("add-interred-users", res.data);
            })
    }

    handleGenderChange = (value) => {
        console.log('selected', value);
        this.setState({ new_gender: value })
    }

    handleMassTimeChange = (value) => {
        this.setState({ new_mass_time: value })
    }

    handleMaritalStatusChange = (value) => {
        this.setState({ new_marital_status: value })
    }
    handleVeteranStatusChange = (value) => {
        this.setState({ new_veteran_status: value })
    }

    handleCemeterySectionsChange = (value) => {
        this.setState({ new_cemetry_section: value })
    }

    handlePropertyStyleChange = (value) => {
        this.setState({ new_property_style: value })
    }

    confirmSubmit = () => {

        if (window.confirm("Are you sure you wish to continue?")) {
            this.setState({ buttontype: "Submit", });
            return true;
        }
        else {
            this.setState({ buttontype: "button", });
            failNotification('bottomLeft')
            return false;

        }
    }



    handleSubmit = (event) => {
        event.preventDefault();

        const inter_fname = event.target.elements.inter_fname.value;
        const inter_lname = event.target.elements.inter_lname.value;
        const gender = this.state.new_gender;
        const mass_time = this.state.new_mass_time;
        const marital_status = this.state.new_marital_status;
        const veteran_status = this.state.new_veteran_status;
        const cemetry_section = this.state.new_cemetry_section;
        const property_style = this.state.new_property_style;
        const date_of_death = event.target.elements.date_of_death.value || null;
        const interment_date = event.target.elements.interment_date.value || null;
        const place_of_death = event.target.elements.place_of_death.value;
        const funeral_home_name = event.target.elements.funeral_home_name.value;
        const church_name = event.target.elements.church_name.value;
        const spouse_name = event.target.elements.spouse_name.value;
        const notes = event.target.elements.notes.value;
        const created_by = this.state.new_created_by;
        const date_created = event.target.elements.date_created.value || null;
        const LotOwner = this.state.new_owner.value;
        const lot_number = event.target.elements.lot_number.value || null;
        const grave_number = event.target.elements.grave_number.value || null;
        const age = event.target.elements.age.value || null;
        const date_of_birth = event.target.elements.date_of_birth.value || null;
        const birth_place = event.target.elements.birth_place.value;


        return axios.put(`http://127.0.0.1:8000/interred_info/${this.props.match.params.interID}/`, {
            inter_fname: inter_fname,
            inter_lname: inter_lname,
            gender: gender,
            date_of_death: date_of_death,
            interment_date: interment_date,
            place_of_death: place_of_death,
            mass_time: mass_time,
            funeral_home_name: funeral_home_name,
            church_name: church_name,
            marital_status: marital_status,
            veteran_status: veteran_status,
            spouse_name: spouse_name,
            cemetry_section: cemetry_section,
            property_style: property_style,
            notes: notes,
            created_by_id: created_by,
            date_created: date_created,
            LotOwner_id: LotOwner,
            lot_number: lot_number,
            grave_number: grave_number,
            age: age,
            date_of_birth: date_of_birth,
            birth_place: birth_place,

        })
            .then(res => { console.log(res); sentNotification('bottomLeft') })
            .catch(err => { console.log(err); errorNotification('bottomLeft') });


    }

    getValue = (opts, val) => opts.find(o => o.value === val);



    handleLotOwnerChange = (value) => {
        this.setState({ new_owner: value })
        console.log('selected lotowner id', value);
    }

    getOptionList = (data) => {
        if (!data) {
            return [];
        }
        let options = [];
        data.forEach(item => {
            options.push(
                <Option value={item.id} key={item.id}>
                    {item.FirstName}  {item.LastName}
                </Option>
            );
        });
        return options;
    }

    getLotOwnerName = (lotData, lotId) => {
        if (!lotData) {
            return [];
        }
        let name = '';
        lotData.forEach(item => {
            if (item.id === lotId) {
                name = item.FirstName + ' ' + item.LastName
            }
        });
        return name;

    }

    handleCreatedByChange = (value) => {
        this.setState({ new_created_by: value })
        console.log('selected created by', value);
    }

    getCreatedByOptionList = (data) => {
        if (!data) {
            return [];
        }
        let options = [];
        data.forEach(item => {
            options.push(
                <Option value={item.id} key={item.id}>
                    {item.username}
                </Option>
            );
        });
        return options;
    }

    getCreatedByName = (users, userID) => {
        if (!users) {
            return [];
        }
        let name = '';
        users.forEach(item => {
            if (item.id === userID) {
                name = item.username
            }
        });
        return name;

    }

    render() {
        const interred = this.state.inter;
        const owner = this.state.lot_owner;
        const users = this.state.users;
        const { getFieldDecorator } = this.props.form;
        getFieldDecorator('keys', { initialValue: [] });
        return (
            <Form {...layout} name="nest-messages" onSubmit={this.handleSubmit} >
                <div className="top-header">
                    <h1 className="formTitle">Interred Form </h1>
                    <div className="btn-sub">
                        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                            <Button className='submitButton' type="primary" htmlType={this.state.buttontype} onClick={this.confirmSubmit}>
                                Update
                            </Button>
                        </Form.Item>
                    </div>
                </div>
                <div className="first-half">
                    <Form.Item label="First name">
                        {getFieldDecorator('inter_fname', {
                            initialValue: interred.inter_fname || [],
                        })(
                            <Input placeholder='Enter First Name' type="text" name='inter_fname' />
                        )}
                    </Form.Item>

                    <Form.Item label="Last name">
                        {getFieldDecorator('inter_lname', {
                            initialValue: interred.inter_lname || [],
                        })(
                            <Input placeholder='Enter Last Name' type="text" name='inter_lname' />
                        )}
                    </Form.Item>

                    <Form.Item label="Lot Owner">
                        {getFieldDecorator('LotOwner', {
                            initialValue: this.getLotOwnerName(owner, interred.LotOwner),
                        })(
                            <Select
                                name='LotOwner'
                                onChange={this.handleLotOwnerChange}
                                placeholder="Select Lot Owner"
                            >
                                {this.getOptionList(owner)}
                            </Select>
                        )}
                    </Form.Item>

                    <Form.Item label="Add" >
                        <a href={`/tombstone/${interred.id}`}> Tombstone Image</a>

                    </Form.Item>

                    <Form.Item label="Lot Number">
                        {getFieldDecorator('lot_number', {
                            initialValue: interred.lot_number || [],
                        })(
                            <Input placeholder='Enter Lot Number' type="number" name='lot_number' />
                        )}
                    </Form.Item>

                    <Form.Item label="Grave Number">
                        {getFieldDecorator('grave_number', {
                            initialValue: interred.grave_number || [],
                        })(
                            <Input placeholder='Enter Grave Number' type="number" name='grave_number' />
                        )}
                    </Form.Item>

                    <Form.Item label="Gender">
                        {getFieldDecorator('gender', {
                            initialValue: interred.gender || [],
                        })(
                            <Select name='gender' onChange={this.handleGenderChange} placeholder='Select Gender'>
                                <Option value="Male">Male</Option>
                                <Option value="Female">Female</Option>
                            </Select>
                        )}
                    </Form.Item>

                    <Form.Item label="Date of Birth">
                        {getFieldDecorator('date_of_birth', {
                            initialValue: interred.date_of_birth || [],
                        })(
                            <Input type="Date" name='date_of_birth' placeholder='Enter Date of Birth' />
                        )}
                    </Form.Item>

                    <Form.Item label="Date of Death">
                        {getFieldDecorator('date_of_death', {
                            initialValue: interred.date_of_death || [],
                        })(
                            <Input type="Date" name='date_of_death' placeholder='Enter Date of Death' />
                        )}
                    </Form.Item>

                    <Form.Item label="Age:">
                        {getFieldDecorator('age', {
                            initialValue: interred.age || [],
                        })(
                            <Input type="number" name='age' placeholder='Enter Age' />
                        )}
                    </Form.Item>

                    <Form.Item label="Place of Birth">
                        {getFieldDecorator('birth_place', {
                            initialValue: interred.birth_place || [],
                        })(
                            <Input placeholder='Enter Place of Birth' type="text" name='birth_place' />
                        )}
                    </Form.Item>
                    <Form.Item label="Buried Date">
                        {getFieldDecorator('interment_date', {
                            initialValue: interred.interment_date || [],
                        })(
                            <Input type="Date" name='interment_date' placeholder='Enter Buried Date' />
                        )}
                    </Form.Item>


                </div>

                <div className="sec-half">


                    <Form.Item label="Place of Death">
                        {getFieldDecorator('place_of_death', {
                            initialValue: interred.place_of_death || [],
                        })(
                            <Input placeholder='Enter Place of Death' type="text" name='place_of_death' />
                        )}
                    </Form.Item>
                    <Form.Item label="Funeral Home Name" >
                        {getFieldDecorator('funeral_home_name', {
                            initialValue: interred.funeral_home_name || [],
                        })(
                            <Input type="text" name='funeral_home_name' placeholder='Enter Funeral Home name' />
                        )}
                    </Form.Item>

                    <Form.Item label="Name of the Church">
                        {getFieldDecorator('church_name', {
                            initialValue: interred.church_name || [],
                        })(
                            <Input placeholder='Enter Name of Church' type="text" name='church_name' />
                        )}
                    </Form.Item >

                    <Form.Item label="Mass Time">
                        {getFieldDecorator('mass_time', {
                            initialValue: interred.mass_time || [],
                        })(
                            <Select name='mass_time' onChange={this.handleMassTimeChange} placeholder='Enter Mass Time'>
                                <Option value="8:00">8:00</Option>
                                <Option value="8:30">8:30</Option>
                                <Option value="9:00">9:00</Option>
                                <Option value="9:30">9:30</Option>
                                <Option value="10:00">10:00</Option>
                                <Option value="10:30">10:30</Option>
                                <Option value="11:00">11:00</Option>
                                <Option value="11:30">11:30</Option>
                                <Option value="12:00">12:00</Option>
                                <Option value="12:30">12:30</Option>
                                <Option value="1:00">1:00</Option>
                                <Option value="1:30">1:30</Option>
                                <Option value="2:00">2:00</Option>
                                <Option value="2:30">2:30</Option>
                                <Option value="3:00">3:00</Option>
                                <Option value="3:30">3:30</Option>
                            </Select>
                        )}

                    </Form.Item>
                    <Form.Item label="Marital Status">
                        {getFieldDecorator('marital_status', {
                            initialValue: interred.marital_status || [],
                        })(
                            <Select name='marital_status' onChange={this.handleMaritalStatusChange} placeholder='Enter Marital Status'>
                                <Option value="Married">Married</Option>
                                <Option value="Single">Single</Option>
                                <Option value="Divorced">Divorced</Option>
                                <Option value="Widow">Widow</Option>
                                <Option value="Unknown">Unknown</Option>
                            </Select>
                        )}
                    </Form.Item>
                    <Form.Item label="Veteran">
                        {getFieldDecorator('veteran_status', {
                            initialValue: interred.veteran_status || [],
                        })(
                            <Select name='veteran_status' onChange={this.handleVeteranStatusChange} placeholder='Enter Veteran Status'>
                                <Option value="Yes">Yes</Option>
                                <Option value="No">No</Option>
                            </Select>
                        )}
                    </Form.Item>

                    <Form.Item label="Section of Cemetery">
                        {getFieldDecorator('cemetry_section', {
                            initialValue: interred.cemetry_section || [],
                        })(
                            <Select name='cemetry_section' onChange={this.handleCemeterySectionsChange} placeholder='Enter Cemetery Section'>
                                <Option value="BCE"> BC E (Baby Cemetery East)</Option>
                                <Option value="BCW"> BC W (Baby Cemetery West)</Option>
                                <Option value="NCA"> NC A (New Cemetery A)</Option>
                                <Option value="NCB"> NC B (New Cemetery B)</Option>
                                <Option value="NCC"> NC C (New Cemetery C)</Option>
                                <Option value="NCD"> NC D (New Cemetery D)</Option>
                                <Option value="NCE"> NC E (New Cemetery E)</Option>
                                <Option value="NCF"> NC F (New Cemetery F)</Option>
                                <Option value="NCG"> NC G (New Cemetery G)</Option>
                                <Option value="NCH"> NC H (New Cemetery H)</Option>
                                <Option value="NCI"> NC I (New Cemetery I)</Option>
                                <Option value="OC"> OC (Old Cemetery)</Option>
                                <Option value="OS"> OC (Old Section)</Option>
                                <Option value="OSM"> OS Memory Garden</Option>
                            </Select>
                        )}
                    </Form.Item>

                    <Form.Item label="Property Style:">
                        {getFieldDecorator('property_style', {
                            initialValue: interred.property_style || [],
                        })(
                            <Select name='property_style' onChange={this.handlePropertyStyleChange} placeholder='Enter Property Style'>
                                <Option value="Burial Lot">Burial Lot</Option>
                                <Option value="Mausoleum">Mausoleum</Option>
                            </Select>
                        )}
                    </Form.Item>
                    <Form.Item label="Name of Spouse">
                        {getFieldDecorator('spouse_name', {
                            initialValue: interred.spouse_name || [],
                        })(
                            <Input placeholder='Enter Name of Spouse' type="text" name='spouse_name' />
                        )}
                    </Form.Item>


                    <Form.Item label="Notes">
                        {getFieldDecorator('notes', {
                            initialValue: interred.notes || [],
                        })(
                            <TextArea placeholder='Enter Notes here' name='notes' />
                        )}
                    </Form.Item>

                    <Form.Item label="Created by">
                        {getFieldDecorator('created_by', {
                            initialValue: this.getCreatedByName(users, interred.created_by),
                        })(
                            <Select
                                name='LotOwner'
                                onChange={this.handleCreatedByChange}
                                placeholder='Select User'
                            >
                                {this.getCreatedByOptionList(users)}
                            </Select>
                        )}
                    </Form.Item>

                    <Form.Item label="Date">
                        {getFieldDecorator('Date', {
                            initialValue: interred.date_created || [],
                        })(
                            <Input type="Date" name='date_created' />
                        )}
                    </Form.Item>
                </div>
            </Form>
        );
    }
}

const WrappedInterredEditForm = Form.create()(InterDetails);
export default WrappedInterredEditForm;
