import React from 'react';
import axios from 'axios';
import Inters from "../../components/Interred"
import './styles.scss'

const listData = [];
for (let i = 0; i < 23; i++) {
    listData.push({
        href: 'http://ant.design',
        title: `ant design part ${i}`,
        avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
        description:
            'Ant Design, a design language for background applications, is refined by Ant UED Team.',
        content:
            'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
    });
}

class InterList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            inters: [],
        }
    };

    componentDidMount() {
        axios.get('http://localhost:8000/interred_info/')
            .then(res => {
                this.setState
                    ({
                        inters: res.data
                    })
                console.log(res.data)
            })
    }
    render() {
        return (
            <div>
                <Inters data={this.state.inters} />
                  {/* <PopUpForm /> */}
            </div>
        );
    }
}

export default InterList;
