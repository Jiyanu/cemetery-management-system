import React from 'react'
//import { useHistory } from "react-router-dom";
import { Form, Input, Button, notification } from 'antd';
import "./picture.css";
import axios from 'axios';

const layout = {

    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 10,
    },
};


const errorNotification = (placement) => {
    notification.open({
        message: `Bad Request`,
        description:
        'Error resulted in submission.Please try again.',placement,
        onClick: () => {
            console.log('Notification Clicked!');

        },
    });
};

const failNotification = (placement) => {
    notification.open({
        message: `Submission Cancelled`,
        description:
        'User decided to halt submission.',placement,
        onClick: () => {
            console.log('Notification Clicked!');
        },
    });
};

const sentNotification = (placement) => {
    notification.open({
        message: `Submission Successfull`,
        description:
        'Details have been sent.',placement,
        onClick: () => {
            console.log('Notification Clicked!');
        },
    });
};


class ImageUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            drawingfile: '',
            picturefile: '',
            imagePreviewUrldrawing: '',
            imagePreviewUrlpicture: '',
            approval_date: null,
            received_date: null,
            tombstone_drawing: null,
            tombstone_picture: null,
            marker_dealer_name: null,
            buttontype:"button",
        };

    }

    _handleDrawingImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                drawingfile: file,
                imagePreviewUrldrawing: reader.result
            });
        }

        reader.readAsDataURL(file)
    }
    _handlePictureImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({

                picturefile: file,
                imagePreviewUrlpicture: reader.result
            });
        }

        reader.readAsDataURL(file)
    }

    componentDidMount() {
        const interID = this.props.match.params.interID;
        axios.get(`http://127.0.0.1:8000/interred_info/${interID}`)
            .then(res => {
                this.setState({
                    inter: res.data
                });
                console.log('response', res.data)
            })
    }

    confirmSubmit=() =>
    {

        if (window.confirm("Are you sure you wish to continue?")){
            this.setState({buttontype: "Submit",});
            return true ;
        }
        else{
            this.setState({buttontype: "button",});
            failNotification('bottomLeft')
            return false ;

        }
    }

    handleSubmit = (event) => {
        console.log("hi")
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {

                //const interred = event.target.elements.interred.value;
                const inter_id = event.target.elements.inter_id.value;
                const approval_date = event.target.elements.approval_date.value;
                const received_date = event.target.elements.received_date.value;
                const tombstone_drawing = event.target.elements.tombstone_drawing.files[0];
                const tombstone_picture = event.target.elements.tombstone_picture.files[0];
                const marker_dealer_name = event.target.elements.marker_dealer_name.value;
                console.log(inter_id)
                console.log(approval_date)
                console.log(tombstone_drawing)
                const uploadData = new FormData();
                uploadData.append('inter_id', inter_id)
                uploadData.append('approval_date', approval_date)
                uploadData.append('received_date', received_date)
                if (tombstone_drawing) {
                    uploadData.append('tombstone_drawing', tombstone_drawing, tombstone_drawing.name)
                }
                if (tombstone_picture) {
                    uploadData.append('tombstone_picture', tombstone_picture, tombstone_picture.name)
                }
                uploadData.append('marker_dealer_name', marker_dealer_name)
                return axios.post('http://127.0.0.1:8000/tombstone/', uploadData, {
                    onUploadProgress: progressEvent => {
                        console.log('Upload Progress:' + Math.round((progressEvent.loaded / progressEvent.total) * 100) + '%')
                    }
                })
                    .then(rsp => { console.log(rsp); sentNotification('bottomLeft') })
                    .catch(err => { console.log(err); errorNotification('bottomLeft') });
            }
            else {
                errorNotification('bottomLeft')
            };
        });
    }
    render() {
        let { imagePreviewUrldrawing } = this.state;
        let { imagePreviewUrlpicture } = this.state;
        let $imagePreviewdrawing, $imagePreviewpicture = null;
        if (imagePreviewUrldrawing) {
            $imagePreviewdrawing = (<img src={imagePreviewUrldrawing} alt="imagePreviewUrldrawing" />);
        } else {
            $imagePreviewdrawing = (<div className="previewText">Please select an Image for Preview</div>);
        }
        if (imagePreviewUrlpicture) {
            $imagePreviewpicture = (<img src={imagePreviewUrlpicture} alt="imagePreviewUrlpicture" />);
        } else {
            $imagePreviewpicture = (<div className="previewText">Please select an Image for Preview</div>);
        }

        return (
            <div className="structure"><br /><br />
                <Form {...layout} name="nest-messages" onSubmit={(e) => this.handleSubmit(e)} >

                    <Form.Item label="Interred ID">
                        <Input type='number' name='inter_id' value={this.props.match.params.interID} />
                    </Form.Item>
                    <Form.Item
                    label="Drawing of marker or tombstone" encType="multipart/form-data"
                >
                        <Input className="fileInput"
                        type="file" name="tombstone_drawing"
                        onChange={(e) => this._handleDrawingImageChange(e)} />
                        <div className="imgPreview">{$imagePreviewdrawing}</div>
                    </Form.Item>
                    <Form.Item
                    label="Picture of marker or tombstone" encType="multipart/form-data"
                >
                        <Input className="fileInput"
                        type="file" name="tombstone_picture"
                        onChange={(e) => this._handlePictureImageChange(e)} />
                        <div className="imgPreview">{$imagePreviewpicture}</div>

                    </Form.Item>
                    <Form.Item label="Approval Date" >
                        <Input type='date' name="approval_date" />
                    </Form.Item>
                    <Form.Item label="Recieved Date" >
                        <Input type='date' name="received_date" />
                    </Form.Item>
                    <Form.Item label="Name of Marker Dealer" >
                        <Input type='text' name="marker_dealer_name" />
                    </Form.Item>

                    <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                        <Button type="primary" htmlType={this.state.buttontype} onClick={this.confirmSubmit}>
                            Submit
                        </Button>
                    </Form.Item>
                </Form>

            </div>

        )
    }
}

const WrappedImageForm = Form.create()(ImageUpload);
export default WrappedImageForm;
