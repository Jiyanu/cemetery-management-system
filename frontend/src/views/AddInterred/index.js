import React from "react";
// import Select from "react-select";
import { Form, Input, Button, notification, } from 'antd';
import "./styles.scss";
import axios from 'axios';
import { Select } from 'antd';
import TextArea from "antd/lib/input/TextArea";
const Option = Select.Option;



const layout = {

    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 10,
    },
};


const errorNotification = (placement) => {
    notification.open({
        message: `Oops!`,
        description:
            'Something went wrong. Please try again.', placement,
        onClick: () => {
            console.log('Notification Clicked!');

        },
    });
};

const failNotification = (placement) => {
    notification.open({
        message: `Submission Cancelled`,
        description:
            'User decided to halt submission.', placement,
        onClick: () => {
            console.log('Notification Clicked!');
        },
    });
};

const sentNotification = (placement) => {
    notification.open({
        message: `Success!`,
        description:
            'Details have been sent.', placement,
        onClick: () => {
            console.log('Notification Clicked!');
        },
    });
};


class AddInterred extends React.PureComponent {

    constructor(props, context) {
        super(props, context);

        this.state = {
            new_created_by: null,
            users: [],
            new_gender: "",
            new_mass_time: "",
            new_marital_status: "",
            new_veteran_status: "",
            new_cemetry_section: "",
            new_property_style: "",
            lot_owner: "",
            new_owner: "",
            buttontype: "button",

        }


    };



    componentDidMount() {
        axios.get(`http://127.0.0.1:8000/lot_owner/`)
            .then(res => {
                this.setState({
                    lot_owner: res.data,
                });
                console.log("add-interred-lot-owner", res.data);
            })

        axios.get(`http://127.0.0.1:8000/users/`)
            .then(res => {
                this.setState({
                    users: res.data,
                });
                console.log("add-interred-users", res.data);
            })

    }

    handleGenderChange = (value) => {
        console.log('selected', value);
        this.setState({ new_gender: value })
    }

    handleMassTimeChange = (value) => {
        this.setState({ new_mass_time: value })
    }

    handleMaritalStatusChange = (value) => {
        this.setState({ new_marital_status: value })
    }
    handleVeteranStatusChange = (value) => {
        this.setState({ new_veteran_status: value })
    }

    handleCemeterySectionsChange = (value) => {
        this.setState({ new_cemetry_section: value })
    }

    handlePropertyStyleChange = (value) => {
        this.setState({ new_property_style: value })
    }


    confirmSubmit = () => {

        if (window.confirm("Are you sure you wish to continue?")) {
            this.setState({ buttontype: "Submit", });
            return true;
        }
        else {
            this.setState({ buttontype: "button", });
            failNotification('bottomLeft')
            return false;

        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log('hi')



        this.props.form.validateFields((err) => {

            if (!err) {

                const inter_fname = event.target.elements.inter_fname.value;
                const inter_lname = event.target.elements.inter_lname.value;
                const gender = this.state.new_gender;
                const mass_time = this.state.new_mass_time;
                const marital_status = this.state.new_marital_status;
                const veteran_status = this.state.new_veteran_status;
                const cemetry_section = this.state.new_cemetry_section;
                const property_style = this.state.new_property_style;
                const date_of_death = event.target.elements.date_of_death.value || null;
                const interment_date = event.target.elements.interment_date.value || null;
                const place_of_death = event.target.elements.place_of_death.value;
                const funeral_home_name = event.target.elements.funeral_home_name.value;
                const church_name = event.target.elements.church_name.value
                const spouse_name = event.target.elements.spouse_name.value;
                const notes = event.target.elements.notes.value;
                const created_by = this.state.new_created_by;
                const date_created = event.target.elements.date_created.value || null;
                const LotOwner = this.state.new_owner;
                const lot_number = event.target.elements.lot_number.value || null;
                const grave_number = event.target.elements.grave_number.value || null;
                const age = event.target.elements.age.value || null;
                const date_of_birth = event.target.elements.date_of_birth.value || null;
                const birth_place = event.target.elements.birth_place.value;
                return axios.post('http://127.0.0.1:8000/interred_info/', {
                    inter_fname: inter_fname,
                    inter_lname: inter_lname,
                    gender: gender,
                    date_of_death: date_of_death,
                    interment_date: interment_date,
                    place_of_death: place_of_death,
                    mass_time: mass_time,
                    funeral_home_name: funeral_home_name,
                    church_name: church_name,
                    marital_status: marital_status,
                    veteran_status: veteran_status,
                    spouse_name: spouse_name,
                    cemetry_section: cemetry_section,
                    property_style: property_style,
                    notes: notes,
                    created_by_id: created_by,
                    date_created: date_created,
                    LotOwner_id: LotOwner,
                    lot_number: lot_number,
                    grave_number: grave_number,
                    age: age,
                    date_of_birth: date_of_birth,
                    birth_place: birth_place,

                })
                    .then(res => { console.log(res); sentNotification('bottomLeft') }) // message.success('Click on Yes')
                    .catch(err => { console.log(err); });
            }
            else {
                errorNotification('bottomLeft')
            }
        });
    }


    handleLotOwnerChange = (value) => {
        this.setState({ new_owner: value })
        console.log('selected lotowner id', value);
    }

    getOptionList = (data) => {
        if (!data) {
            return [];
        }
        let options = [];
        data.forEach(item => {
            options.push(
                <Option value={item.id} key={item.id}>
                    {item.FirstName}  {item.LastName}
                </Option>
            );
        });
        return options;
    }

    handleCreatedByChange = (value) => {
        this.setState({ new_created_by: value })
        console.log('selected created by', value);
    }

    getCreatedByOptionList = (data) => {
        if (!data) {
            return [];
        }
        let options = [];
        data.forEach(item => {
            options.push(
                <Option value={item.id} key={item.id}>
                    {item.username}
                </Option>
            );
        });
        return options;
    }
    render() {

        const owner = this.state.lot_owner;
        const users = this.state.users;
        const { getFieldDecorator } = this.props.form;

        return (
            <Form {...layout} name="nest-messages" onSubmit={this.handleSubmit} >
                <div className="top-header">
                    <h1 className="formTitle">New Interred Form</h1>
                    <div className="btn-sub">
                        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                            <Button className='submitButton' type="primary" htmlType={this.state.buttontype} onClick={this.confirmSubmit}>
                                Submit
                            </Button>
                        </Form.Item>
                    </div>
                </div>
                <div className="first-half">
                    <Form.Item label="First name">
                        {getFieldDecorator(`inter_fname`, {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                    message: 'First name missing'
                                },
                            ],
                        })(
                            <Input placeholder='Enter First Name' type="text" pattern='[a-zA-Z]+' name='inter_fname' />
                        )}
                    </Form.Item>

                    <Form.Item label="Last name">
                        {getFieldDecorator(`inter_lname`, {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                    message: 'Last name missing'
                                },
                            ],
                        })(
                            <Input placeholder='Enter Last Name' type="text" pattern='[a-zA-Z\-]+' name='inter_lname' />
                        )}
                    </Form.Item>

                    <Form.Item label="Lot Owner">
                        <Select
                            name='LotOwner'
                            onChange={this.handleLotOwnerChange}
                            placeholder="Select Lot Owner"
                        >
                            {this.getOptionList(owner)}
                        </Select>
                    </Form.Item>

                    <Form.Item label="Add" >
                        <a href="/add-tombstone"> Tombstone Image</a>
                    </Form.Item>

                    <Form.Item label="Lot Number">
                        <Input placeholder='Enter Lot Number' type="text" pattern='[0-9]+' name='lot_number' />
                    </Form.Item>

                    <Form.Item label="Grave Number">
                        <Input placeholder='Enter Grave Number' type="number" pattern='[0-9]+' name='grave_number' />
                    </Form.Item>

                    <Form.Item label="Gender">
                        <Select name='gender' placeholder='Select Gender' onChange={this.handleGenderChange}>
                            <Option value="Male">Male</Option>
                            <Option value="Female">Female</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item label="Date of Birth">
                        <Input type="Date" name='date_of_birth' />
                    </Form.Item>

                    <Form.Item label="Date of Death">
                        <Input type="Date" name='date_of_death' />
                    </Form.Item>

                    <Form.Item label="Age:">
                        <Input placeholder='Enter Age' type="number" name='age' />
                    </Form.Item>

                    <Form.Item label="Place of Birth">
                        <Input placeholder='Enter Place of Birth' type="text" name='birth_place' />
                    </Form.Item>

                    <Form.Item label="Buried Date">
                        <Input type="Date" name='interment_date' />
                    </Form.Item>



                </div>

                <div className="sec-half">
                    <Form.Item label="Place of Death">
                        <Input placeholder='Enter Place of Death' type="text" name='place_of_death' />
                    </Form.Item>

                    <Form.Item label="Mass Time">
                        <Select name='mass_time' onChange={this.handleMassTimeChange} placeholder="Select Mass Time">
                            <Option value="8:00">8:00</Option>
                            <Option value="8:30">8:30</Option>
                            <Option value="9:00">9:00</Option>
                            <Option value="9:30">9:30</Option>
                            <Option value="10:00">10:00</Option>
                            <Option value="10:30">10:30</Option>
                            <Option value="11:00">11:00</Option>
                            <Option value="11:30">11:30</Option>
                            <Option value="12:00">12:00</Option>
                            <Option value="12:30">12:30</Option>
                            <Option value="1:00">1:00</Option>
                            <Option value="1:30">1:30</Option>
                            <Option value="2:00">2:00</Option>
                            <Option value="2:30">2:30</Option>
                            <Option value="3:00">3:00</Option>
                            <Option value="3:30">3:30</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item label="Funeral Home Name">
                        <Input placeholder='Enter Funeral Home Name' type="text" pattern="pattern=[a-zA-Z\s\-\.\']+" name='funeral_home_name' />
                    </Form.Item>

                    <Form.Item label="Name of the Church">
                        <Input placeholder='Enter Name of Church' type="text" pattern="[a-zA-Z\s\-\.\']+" name='church_name' />
                    </Form.Item >

                    <Form.Item label="Marital Status">
                        <Select name='marital_status' onChange={this.handleMaritalStatusChange} placeholder="Select Marital Status" >
                            <Option value="Married">Married</Option>
                            <Option value="Single">Single</Option>
                            <Option value="Divorced">Divorced</Option>
                            <Option value="Widow">Widow</Option>
                            <Option value="Unknown">Unknown</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item label="Veteran">

                        <Select placeholder="Select Veteran Status" name='veteran_status' onChange={this.handleVeteranStatusChange}>
                            <Option value="Yes">Yes</Option>
                            <Option value="No">No</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item label="Name of Spouse">
                        <Input placeholder='Enter Name of Spouse' type="text" pattern="[a-zA-Z\s\-\.\']+" name='spouse_name' />
                    </Form.Item>

                    <Form.Item label="Section of Cemetery" >
                        <Select name='cemetry_section' placeholder="Selection Cemetery Section" onChange={this.handleCemeterySectionsChange}>
                            <Option value="BC E (Baby Cemetery East)"> BC E (Baby Cemetery East)</Option>
                            <Option value="BC W (Baby Cemetery West)"> BC W (Baby Cemetery West)</Option>
                            <Option value="NC A (New Cemetery A)"> NC A (New Cemetery A)</Option>
                            <Option value="NC B (New Cemetery B)"> NC B (New Cemetery B)</Option>
                            <Option value="NC C (New Cemetery C)"> NC C (New Cemetery C)</Option>
                            <Option value="NC D (New Cemetery D)"> NC D (New Cemetery D)</Option>
                            <Option value="NC E (New Cemetery E)"> NC E (New Cemetery E)</Option>
                            <Option value="NC F (New Cemetery F)"> NC F (New Cemetery F)</Option>
                            <Option value="NC G (New Cemetery G)"> NC G (New Cemetery G)</Option>
                            <Option value="NC H (New Cemetery H)"> NC H (New Cemetery H)</Option>
                            <Option value="NC I (New Cemetery I)"> NC I (New Cemetery I)</Option>
                            <Option value="OC (Old Cemetery)"> OC (Old Cemetery)</Option>
                            <Option value="OC (Old Section)"> OC (Old Section)</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item label="Property Style:">
                        <Select placeholder="Select Property Style" name='property_style' onChange={this.handlePropertyStyleChange}>
                            <Option value="Burial Lot">Burial Lot</Option>
                            <Option value="Mausoleum">Mausoleum</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item label="Notes">
                        <TextArea placeholder='Enter Notes here' name='notes' />
                    </Form.Item>

                    <Form.Item label="Created by">
                        <Select
                            name='created_by'
                            onChange={this.handleCreatedByChange}
                            placeholder="Select User"
                        >
                            {this.getCreatedByOptionList(users)}
                        </Select>
                    </Form.Item>

                    <Form.Item label="Date">
                        <Input type="Date" name='date_created' />
                    </Form.Item>
                </div>
            </Form>
        );
    }
}
const WrappedInterredForm = Form.create()(AddInterred);
export default WrappedInterredForm;
