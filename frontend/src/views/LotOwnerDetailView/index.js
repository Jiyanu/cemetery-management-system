import React from 'react';
import axios from 'axios';
import { Form, Input, Button,notification } from 'antd';
import './styles.scss'

let id = 0;
const layout = {

    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 10,
    },
};

const errorNotification = (placement) => {
  notification.open({
    message: `Oops!`,
    description:
      'Something went wrong. Please try again.',placement,
    onClick: () => {
      console.log('Notification Clicked!');

    },
  });
};

const failNotification = (placement) => {
  notification.open({
    message: `Submission Cancelled`,
    description:
      'User decided to halt submission.',placement,
    onClick: () => {
      console.log('Notification Clicked!');
    },
  });
};

const sentNotification = (placement) => {
  notification.open({
    message: `Success!`,
    description:
      'Details have been sent.',placement,
    onClick: () => {
      console.log('Notification Clicked!');
    },
  });
};


class LotOwnerDetailView extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            lot_owner: [],
            lengthInterred: 0,
            inter_data: []
        }
    };
    componentDidMount() {
        const lotOwnerID = this.props.match.params.lotOwnerID;
        axios.get(`http://127.0.0.1:8000/lot_owner/${lotOwnerID}`)
            .then(res => {
                this.setState({
                    lot_owner: res.data,
                    inter_data: res.data.interreds,
                });
                console.log(res.data)
            })
    }


    remove = k => {
        const { form } = this.props;

        const keys = form.getFieldValue('keys');
        if (keys.length === 1) {
            return;
        }


        form.setFieldsValue({
            keys: keys.filter(key => key !== k),
        });
    };

    add = () => {
        const { form } = this.props;
        const keys = form.getFieldValue('keys');
        const nextKeys = keys.concat(id++);

        form.setFieldsValue({
            keys: nextKeys,
        });

        this.setState({
            lengthInterred: this.state.lengthInterred + 1,
        });

    };
    confirmSubmit=() =>
    {

      if (window.confirm("Are you sure you wish to continue?")){
        this.setState({buttontype: "submit",});
      return true ;
      }
      else{
        this.setState({buttontype: "button",});
        failNotification('bottomLeft')
        return false ;

      }
    }


    handleSubmit = (event) => {
        const interred_length = this.state.inter_data.length;
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const interreds = [];
                for (let i = 0; i < interred_length; i += 1) {
                    interreds.push({
                        inter_fname: values.interreds[i]["inter_fname"],
                        inter_lname: values.interreds[i]["inter_lname"],
                    });
                }


                const FirstName = event.target.elements.FirstName.value;
                const LastName = event.target.elements.LastName.value;
                // const InterFname = event.target.elements.inter_fname.value;
                // const InterLname = event.target.elements.inter_lname.value;
                const StreetAddress = event.target.elements.StreetAddress.value;
                const AptNo = event.target.elements.AptNo.value;
                const City = event.target.elements.City.value;
                const State = event.target.elements.State.value;
                const Country = event.target.elements.Country.value;
                const Zip = event.target.elements.Zip.value;
                const PhoneNumber = event.target.elements.PhoneNumber.value;
                const Cost = event.target.elements.Cost.value;
                const DateIssued = event.target.elements.DateIssued.value || null;
                const TotalNoOfLots = event.target.elements.TotalNoOfLots.value;
                // InterFname: InterFname,
                //    InterLname: InterLname,
                return axios.put(`http://127.0.0.1:8000/lot_owner/${this.props.match.params.lotOwnerID}/`, {
                    FirstName: FirstName,
                    LastName: LastName,
                    StreetAddress: StreetAddress,
                    AptNo: AptNo,
                    City: City,
                    State: State,
                    Country: Country,
                    Zip: Zip,
                    PhoneNumber: PhoneNumber,
                    DateIssued: DateIssued,
                    Cost: Cost,
                    TotalNoOfLots: TotalNoOfLots,
                    interreds: interreds || null
                })
                .then(res => { console.log(res);  sentNotification('bottomLeft')})
                .catch(err => { console.log(err);   errorNotification('bottomLeft') });
            }
            else {
              errorNotification('bottomLeft')
            }

        });
    }

    render() {
        const inter = this.state.inter_data;
        const owner = this.state.lot_owner;
        const { getFieldDecorator } = this.props.form;
        getFieldDecorator('keys', { initialValue: [] });
        const formItems = inter.map((interred, key) => (
            <Form.Item
                required={true}
                key={key}
            >
                {getFieldDecorator(`interreds[${key}][inter_fname]`, {
                    initialValue: interred.inter_fname || [],
                }, {
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [
                        {
                            required: true,
                            whitespace: true,
                        },
                    ],
                })(<Input placeholder="Interred First Name" />)}

                {getFieldDecorator(`interreds[${key}][inter_lname]`, {
                    initialValue: interred.inter_lname || [],
                }, {
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [
                        {
                            required: true,
                            whitespace: true,
                        },
                    ],
                })(<Input placeholder="Interred Last Name" />)}



            </Form.Item>
        ));
        return (
            <Form {...layout} name="nest-messages" onSubmit={this.handleSubmit} >
                <div className="top-header">
                    <h1 className="formTitle">Lot Owner Form </h1>
                    <div className="btn-sub">
                        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                            <Button htmlType={this.state.buttontype} onClick={this.confirmSubmit} type="primary" className="Form_Field_Button">
                                Update
                            </Button>
                        </Form.Item>
                    </div>
                </div>
                <div className="first-half">
                    <Form.Item label="First name" className="Form_Field" >
                        {getFieldDecorator('FirstName', {
                            initialValue: owner.FirstName || [],
                        })(
                            <Input placeholder="Enter First Name" name="FirstName" type="text" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Last name" className="Form_Field" >
                        {getFieldDecorator('LastName', {
                            initialValue: owner.LastName || [],
                        })(
                            <Input placeholder="Enter Last Name" name="LastName" type="text" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    {this.state.inter_data.length > 0 ?
                    <React.Fragment>
                        <Form.Item label="Interred Details" >
                            {inter.map((interred, key) => (
                                <span key={key}>
                                    <a href={`/ interred / ${interred.id}`}>{interred.inter_fname} {interred.inter_lname}</a>
                                    <br />
                                </span>
                            ))}
                            </Form.Item>

                            <Form.Item label="Interred Details" >
                                <div className="add-btn">{formItems} </div>
                            </Form.Item>
                    </React.Fragment>
                    :
                            <Form.Item label="Interred Details" >
                                <span style={{color: "blue"}}> No interreds found </span>
                            </Form.Item>
                    }

                    <Form.Item label="Street Address" className="Form_Field">
                        {getFieldDecorator('StreetAddress', {
                            initialValue: owner.StreetAddress || [],
                        })(
                            <Input type="text" placeholder="Enter Street Address" name="StreetAddress" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Apt #" className="Form_Field">
                        {getFieldDecorator('AptNo', {
                            initialValue: owner.AptNo || [],
                        })(
                            <Input type="number" placeholder="Optional: Enter Apartment Number" name="AptNo" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="City" className="Form_Field">
                        {getFieldDecorator('City', {
                            initialValue: owner.City || [],
                        })(
                            <Input placeholder="Enter Name of City " name="City" type="text" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="State" className="Form_Field">
                        {getFieldDecorator('State', {
                            initialValue: owner.State || [],
                        })(
                            <Input placeholder="Enter State" name="State" type="text" className="Form_Field_Input" />
                        )}
                    </Form.Item>
                </div>

                <div className="sec-half">
                    <Form.Item label="Country" className="Form_Field">
                        {getFieldDecorator('Country', {
                            initialValue: owner.Country || [],
                        })(
                            <Input placeholder="Enter Country of Residence" name="Country" type='[a-zA-Z]+' className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Zip" className="Form_Field">
                        {getFieldDecorator('Zip', {
                            initialValue: owner.Zip || [],
                        })(
                            <Input placeholder="Enter Postal Code" name="Zip" type="number" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Phone Number" className="Form_Field">
                        {getFieldDecorator('PhoneNumber', {
                            initialValue: owner.PhoneNumber || [],
                        })(
                            <Input placeholder="Enter Telephone Number" name="PhoneNumber" type="number" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Cost" className="Form_Field">
                        {getFieldDecorator('Cost', {
                            initialValue: owner.Cost || [],
                        })(
                            <Input placeholder="Enter Cost of Purchased Lots" name="Cost" pattern='^\d*(\.\d{0,2})?$' className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Date Issued: " className="Form_Field">
                        {getFieldDecorator('DataIssued', {
                            initialValue: owner.DateIssued || [],
                        })(
                            <Input type="Date" name="DateIssued" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="No. of Lots Purchased: " className="Form_Field">
                        {getFieldDecorator('TotalNoOfLots', {
                            initialValue: owner.TotalNoOfLots || [],
                        })(
                            <Input type="number" name="TotalNoOfLots" placeholder="Enter No. of Lots Purchased" className="Form_Field_Input" />
                        )}
                    </Form.Item>
                </div>
            </Form>
        );
    }
}

const WrappedOwnerEditForm = Form.create()(LotOwnerDetailView);
export default WrappedOwnerEditForm;
