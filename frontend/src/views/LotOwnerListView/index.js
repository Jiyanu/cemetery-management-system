import React from "react";
import axios from 'axios';
import LotOwner from '../../components/LotOwner';
//import { SearchOutlined } from '@ant-design/icons';

import './styles.scss'

export default class LotOwnerList extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            data: [],
        }
    };

    componentDidMount() {
        axios.get('http://localhost:8000/lot_owner/')
            .then(res => {
                this.setState({
                    data: res.data,
                });
            })

    }

    render() {
        return (
            <div className="content">
                <LotOwner data={this.state.data} />
                {/* <PopUpForm requestType="post" /> */}
            </div>
        );
    }
}
