// import React from 'react';
// import { Form, Input, Button } from 'antd';
// import { Modal } from 'antd';
// import axios from 'axios';
// import './styles.scss';


// class PopUpForm extends React.Component {


//     formRef = React.createRef();

//     handleFormSubmit = (event) => {

//         const FirstName = event.target.elements.FirstName.value;
//         const LastName = event.target.elements.LastName.value;
//         const StreetAddress = event.target.elements.StreetAddress.value;
//         const AptNo = event.target.elements.AptNo.value;
//         const City = event.target.elements.City.value;
//         const State = event.target.elements.State.value;
//         const Country = event.target.elements.Country.value;
//         const Zip = event.target.elements.Zip.value;
//         const PhoneNumber = event.target.elements.PhoneNumber.value;
//         const Cost = event.target.elements.Cost.value;
//         const DateIssued = event.target.elements.DateIssued.value;
//         const TotalNoOfLots = event.target.elements.TotalNoOfLots.value;


//         return axios.post('http://127.0.0.1:8000/lot_owner/', {
//             FirstName: FirstName,
//             LastName: LastName,
//             StreetAddress: StreetAddress,
//             AptNo: AptNo,
//             City: City,
//             State: State,
//             Country: Country,
//             Zip: Zip,
//             PhoneNumber: PhoneNumber,
//             DateIssued: DateIssued,
//             Cost: Cost,
//             TotalNoOfLots: TotalNoOfLots,
//         })
//             .then(res => console.log(res))
//             .catch(err => console.err(err));

//     }


//     /* onFinish = values => {
//          console.log(values);
//      }; */

//     onReset = () => {
//         this.formRef.current.resetFields();
//     };

//     state = {
//         loading: false,
//         visible: false,
//     };

//     showModal = () => {
//         this.setState({
//             visible: true,
//         });
//     };

//     handleOk = () => {
//         this.setState({ loading: true });
//         setTimeout(() => {
//             this.setState({ loading: false, visible: false });
//         }, 3000);
//     };

//     handleCancel = () => {
//         this.setState({ visible: false });
//     };

//     render() {
//         const { visible } = this.state;
//         return (
//             <div>
//                 <div>
//                     <span className="add-owner" shape="circle" onClick={this.showModal}>
//                         <i className="fa fa-plus my-float"></i>
//                     </span>
//                 </div>
//                 <Modal
//                     visible={visible}
//                     title="Lot Owner Form"
//                     onOk={this.handleOk}
//                     onCancel={this.handleCancel}
//                     footer={[
//                         <Button key="back" onClick={this.handleCancel}>
//                             Return
//                         </Button>

//                     ]}
//                 >
//                     <Form onSubmit={event =>
//                         this.handleFormSubmit(
//                             event
//                         )}
//                     >

//                         < Form.Item label="First name"

//                         >
//                             <Input placeholder="Enter First Name" name="FirstName" />
//                         </Form.Item>
//                         <Form.Item label="Last name"

//                         >
//                             <Input placeholder="Enter Last Name" name="LastName" />
//                         </Form.Item>
//                         <Form.Item label="Street Address"

//                         >
//                             <Input placeholder="Enter Street Address" name="StreetAddress" />
//                         </Form.Item>
//                         <Form.Item label="Apt #"

//                         >
//                             <Input placeholder="Optional: Enter Apartment Number" name="AptNo" />
//                         </Form.Item>
//                         <Form.Item label="City"

//                         >
//                             <Input placeholder="Enter Name of City " name="City" />
//                         </Form.Item>
//                         <Form.Item label="State"

//                         >
//                             <Input placeholder="Enter State Located" name="State" />
//                         </Form.Item>
//                         <Form.Item label="Country"

//                         >
//                             <Input placeholder="Enter Country of Residence" name="Country" />
//                         </Form.Item>
//                         <Form.Item name="Zip" label="Zip"

//                         >
//                             <Input placeholder="Enter Postal Code" />
//                         </Form.Item>
//                         <Form.Item label="Phone Number"

//                         >
//                             <Input placeholder="Enter Telephone Number" name="PhoneNumber" />
//                         </Form.Item>
//                         <Form.Item label="Cost"


//                         >
//                             <Input placeholder="Enter Cost for No. of Lots Purchased" name="Cost" />
//                         </Form.Item>
//                         <Form.Item label="Date Issued: "

//                         >
//                             <Input name="DateIssued" />
//                         </Form.Item>
//                         <Form.Item

//                             label="No. of Lots Purchased: "

//                         >
//                             <Input type="Number" name="TotalNoOfLots" placeholder="Enter No. of Lots Purchased" />
//                         </Form.Item>
//                         <Form.Item >

//                             <Button htmlType="button" onClick={this.onReset}>
//                                 Reset
//                             </Button>
//                             <Button htmlType="submit" type="primary" >
//                                 Submit
//                      </Button>
//                         </Form.Item>

//                     </Form>
//                 </Modal>
//             </div >
//         );
//     }
// }

// export default PopUpForm;
