import React from 'react';
import { Form, Input, Button, Icon, notification } from 'antd';
import axios from 'axios';
import './styles.scss';

let id = 0;

const layout = {

    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 10,
    },
};


const errorNotification = (placement) => {
    notification.open({
        message: `Oops!`,
        description:
            'Something went wrong. Please try again.', placement,
        onClick: () => {
            console.log('Notification Clicked!');

        },
    });
};

const failNotification = (placement) => {
    notification.open({
        message: `Submission Cancelled`,
        description:
            'User decided to halt submission.', placement,
        onClick: () => {
            console.log('Notification Clicked!');
        },
    });
};

const sentNotification = (placement) => {
    notification.open({
        message: `Success!`,
        description:
            'Details have been sent.', placement,
        onClick: () => {
            console.log('Notification Clicked!');
        },
    });
};


class AddLotOwner extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            lengthInterred: 0,
            buttontype: "button",
        }
    };

    add = () => {
        const { form } = this.props;
        const keys = form.getFieldValue('keys');
        const nextKeys = keys.concat(id++);

        form.setFieldsValue({
            keys: nextKeys,
        });

        this.setState({
            lengthInterred: this.state.lengthInterred + 1,
        });
    };

    remove = k => {
        const { form } = this.props;
        const keys = form.getFieldValue("keys");
        if (keys.length === 1) return;
        form.setFieldsValue({
            keys: keys.filter(key => key !== k)
        });
    };
    confirmSubmit = () => {

        if (window.confirm("Are you sure you wish to continue?")) {
            this.setState({ buttontype: "Submit", });
            return true;
        }
        else {
            this.setState({ buttontype: "button", });
            failNotification('bottomLeft')
            return false;

        }
    }


    handleSubmit = (event) => {
        console.log("state-interred-length", this.state.lengthInterred)
        const interred_length = this.state.lengthInterred;
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            console.log('sth', interred_length)
            if (!err) {
                console.log("Received values of form: ", values);
                const interreds = [];
                for (let i = 0; i < interred_length; i += 1) {
                    interreds.push({
                        inter_fname: values.interreds[i]["inter_fname"],
                        inter_lname: values.interreds[i]["inter_lname"],
                    });
                }


                const FirstName = event.target.elements.FirstName.value;
                const LastName = event.target.elements.LastName.value;
                // const InterFname = event.target.elements.inter_fname.value;
                // const InterLname = event.target.elements.inter_lname.value;
                const StreetAddress = event.target.elements.StreetAddress.value;
                const AptNo = event.target.elements.AptNo.value;
                const City = event.target.elements.City.value;
                const State = event.target.elements.State.value;
                const Country = event.target.elements.Country.value;
                const Zip = event.target.elements.Zip.value;
                const PhoneNumber = event.target.elements.PhoneNumber.value;
                const Cost = event.target.elements.Cost.value;
                const DateIssued = event.target.elements.DateIssued.value || null;
                const TotalNoOfLots = event.target.elements.TotalNoOfLots.value;
                // InterFname: InterFname,
                //    InterLname: InterLname,
                return axios.post('http://127.0.0.1:8000/lot_owner/', {
                    FirstName: FirstName,
                    LastName: LastName,
                    StreetAddress: StreetAddress,
                    AptNo: AptNo,
                    City: City,
                    State: State,
                    Country: Country,
                    Zip: Zip,
                    PhoneNumber: PhoneNumber,
                    DateIssued: DateIssued,
                    Cost: Cost,
                    TotalNoOfLots: TotalNoOfLots,
                    interreds: interreds || null
                })
                    .then(rsp => { console.log(rsp); sentNotification('bottomLeft') })
                    .catch(err => { console.log(err); errorNotification('bottomLeft') });
            }
            else {
                errorNotification('bottomLeft')
            }
        });
    }
    render() {
        const { getFieldDecorator, getFieldValue } = this.props.form;
        getFieldDecorator('keys', { initialValue: [] });
        const keys = getFieldValue('keys');
        const formItems = keys.map((k, x, index) => (
            <Form.Item
                required={true}
                key={k} style={{ marginLeft: "30px" }}
            >
                <span>{x + 1} Potential Interred </span>
                {getFieldDecorator(`interreds[${k}][inter_fname]`, {
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [
                        {
                            required: true,
                            whitespace: true,
                            message: 'Fields are required'
                        },
                    ],
                })(<Input placeholder="Interred First Name" pattern='[a-zA-Z]+' />)}

                {getFieldDecorator(`interreds[${k}][inter_lname]`, {
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [
                        {
                            required: true,
                            whitespace: true,
                        },
                    ],
                })(<Input placeholder="Interred Last Name" pattern='[a-zA-Z\-]+' />)}
                {keys.length > 1 ? (
                    <Icon
                        className="dynamic-delete-button"
                        type="minus-circle-o"
                        disabled={keys.length === 1}
                        onClick={() => this.remove(k)}
                    />
                ) : null}
            </Form.Item>
        ));
        return (
            <Form {...layout} className="form-antd" onSubmit={this.handleSubmit} >
                <div className="top-header">
                    <h1 className="formTitle">New Lot Owner Form</h1>
                    <div className="btn-sub">
                        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                            <Button htmlType={this.state.buttontype} onClick={this.confirmSubmit} type="primary" className="Form_Field_Button">
                                Submit
                            </Button>
                        </Form.Item>
                    </div>
                </div>
                <div className="first-half">
                    <Form.Item label="First name" className="Form_Field" >
                        {getFieldDecorator(`FirstName`, {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                },
                            ],
                        })(
                            <Input placeholder="Enter First Name" name="FirstName" type="text" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Last name" className="Form_Field" >
                        {getFieldDecorator('LastName', {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                },
                            ],
                        })(
                            <Input placeholder="Enter Last Name" name="LastName" type="text" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Add Interred Details" className="Form_Field">
                        <Button type="primary" onClick={this.add}>
                            <span>Add</span>
                        </Button>
                    </Form.Item>

                    <div className="add-btn">{formItems} </div>

                    <Form.Item label="Street Address" className="Form_Field">
                        {getFieldDecorator('StreetAddress', {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                },
                            ],
                        })(
                            <Input type="text" placeholder="Enter Street Address" name="StreetAddress" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Apt #" className="Form_Field">
                        <Input type="number" placeholder="Optional: Enter Apartment Number" name="AptNo" className="Form_Field_Input" />
                    </Form.Item>

                    <Form.Item label="City" className="Form_Field">
                        {getFieldDecorator('City', {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                },
                            ],
                        })(
                            <Input placeholder="Enter Name of City " name="City" type="text" pattern="[a-zA-Z\s\-\.\']+" className="Form_Field_Input" />
                        )}
                    </Form.Item>


                </div>

                <div className="sec-half">
                    <Form.Item label="State" className="Form_Field">
                        {getFieldDecorator(`State`, {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                },
                            ],
                        })(
                            <Input placeholder="Enter State" name="State" type="text" pattern='[a-zA-Z\s]+' className="Form_Field_Input" />
                        )}
                    </Form.Item>
                    <Form.Item label="Country" className="Form_Field">
                        {getFieldDecorator(`Country`, {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                },
                            ],
                        })(
                            <Input placeholder="Enter Country of Residence" name="Country" type='[a-zA-Z]+' className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Zip" className="Form_Field">
                        {getFieldDecorator('Zip', {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                },
                            ],
                        })(
                            <Input placeholder="Enter Postal Code" name="Zip" type="number" pattern='[0-9]{5}' className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Phone Number" className="Form_Field">
                        {getFieldDecorator('PhoneNumber', {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                },
                            ],
                        })(
                            <Input placeholder="Enter Telephone Number" name="PhoneNumber" type="number" pattern='^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$' className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Cost" className="Form_Field">
                        {getFieldDecorator('Cost', {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                },
                            ],
                        })(
                            <Input placeholder="Enter Cost of Purchased Lots" name="Cost" pattern='^\d*(\.\d{0,2})?$' className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="Date Issued: " className="Form_Field">
                        {getFieldDecorator('DateIssued', {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                },
                            ],
                        })(
                            <Input type="Date" name="DateIssued" className="Form_Field_Input" />
                        )}
                    </Form.Item>

                    <Form.Item label="No. of Lots Purchased: " className="Form_Field">
                        {getFieldDecorator('TotalNoOfLots', {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                },
                            ],
                        })(
                            <Input type="number" name="TotalNoOfLots" placeholder="Enter No. of Lots Purchased" pattern='[0-9]+' className="Form_Field_Input" />
                        )}
                    </Form.Item>
                </div>
            </Form>
        );
    }
}
const WrappedOwnerForm = Form.create()(AddLotOwner);
export default WrappedOwnerForm;
