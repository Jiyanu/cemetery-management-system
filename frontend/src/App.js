import React from 'react';
import { BrowserRouter as Router,} from "react-router-dom";
import { connect } from 'react-redux';

import 'antd/dist/antd.css';
import 'font-awesome/css/font-awesome.min.css';

import '../src/';
import * as actions from './store/actions/auth';
import Page from './components/Page'
import BaseRouter from './routes.js'

class App extends React.PureComponent {

    componentDidMount() {
        this.props.onTryAutoSignup();
    }
    render() {
        return (
            <Router>
                <Page {...this.props} >
                    <BaseRouter {...this.props} />
                </Page>
            </Router>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.token !== null
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onTryAutoSignup: () => dispatch(actions.authCheckState())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(App);