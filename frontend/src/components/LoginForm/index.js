import React from 'react';
import {connect} from 'react-redux';
import { NavLink, withRouter, } from "react-router-dom";
import { Form, Icon, Input, Button, Spin, } from 'antd';

import * as actions from '../../../src/store/actions/auth.js';

import './styles.scss';

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

class LoginForm extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
        }
    }

    handleSubmit = async(e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            err = this.props.error
            this.props.onAuth(values.username, values.password)
            setTimeout(() => {
                console.log('timeout-error-antd', this.props.error)
                if (!this.props.error) {
                    this.props.history.push('/lot-owner');
                }
            }, 200);
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        let errorMessage = null;
        if (this.props.error) {
            errorMessage = (
                <div className="login-error">
                    Oops! Login Failed.
                    <span className="msg">
                        {this.props.error.message}
                    </span>
                </div>
            );
        }

    return (
        <div className="login">
            <h1 className="text"> Welcome to Saints Peter & Paul Cemetery </h1>
            {errorMessage}
            { this.props.loading ?
            <Spin indicator={antIcon} />
            :
            <Form onSubmit={this.handleSubmit} className="login-form">
                <Form.Item>
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: 'Please input your username!' }],
                    })(
                        <Input
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="Username"
                    />,
                    )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'Please input your Password!' }],
                        })(
                            <Input
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            placeholder="Password"
                        />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Login
                        </Button>
                        <NavLink to='/register/'>
                            <span className="reg" >Register </span>
                        </NavLink>
                    </Form.Item>
            </Form>
            }
        </div>
    );
  }
}

const WrappedNormalLoginForm = Form.create()(LoginForm);

const mapStateToProps = (state) => {
    return {
        loading: state.loading,
        error: state.error,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, password) => dispatch(actions.authLogin(username, password)),
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WrappedNormalLoginForm));
