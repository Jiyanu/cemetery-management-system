import React from "react";
import { Table, Input, Button } from 'antd';
import Highlighter from 'react-highlight-words';
import { CSVLink } from 'react-csv';

import './styles.scss'

export default class Inter extends React.PureComponent {
        constructor(props) {
        super(props);

        this.state = {
            //data: this.props.data,
            searchText: '',
            searchedColumn: '',
            index: 0,
            filteredInfo: {},
            a: '',
            isFiltered: false,
        }
    };

    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                    Reset
                </Button>
            </div>
        ),

        filterIcon: filtered => <span className="search-icon" ><i className="fa fa-search" /></span>,
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text.toString()}
                />
            ) : (
                    text
                ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
      clearFilters();
      this.setState({
          searchText: '',
          isFiltered: false,
      });
  };

    clearAll = () => {
        this.setState({
            searchText: '',
            index: this.state.index + 1,
            isFiltered: false,
        })
    };
    handleChange = (pagination, filters, sorter, extra: { currentDataSource: [] }) => {
        let filteredData = extra.currentDataSource;
        this.setState({
            isFiltered: true,
            filteredInfo: filteredData,
        });
    }

    getCurrentDate(separator=''){

        let newDate = new Date()
        let date = newDate.getDate();
        let month = newDate.getMonth() + 1;
        let year = newDate.getFullYear();
        var s1 = '.csv'
        var s2 = `${year}${separator}${month<10?`0${month}`:`${month}`}${separator}${date}`
        return s2.concat(s1)
    }

    $CSVLink = React.createRef();

    getFileName() {
        let d = new Date();
        let dformat = `${d.getDate()}-${d.getMonth()+1}-${d.getFullYear()}`;
        return "interred-" + dformat + ".csv";
    }

    createdBy = (data) => {
        if(data) {
            return data.username
        }
        else return ""
    }

    render() {

        const { data } = this.props;
        const columns = [
            {
                title: 'Interred ID',
                dataIndex: 'id',
                key: 'id',
                width: '10%',
                defaultSortOrder: 'ascend',
                sorter: (a, b) => a.id - b.id,
                ...this.getColumnSearchProps('id'),
            },
            {
                title: 'First Name',
                dataIndex: 'inter_fname',
                key: 'inter_fname',
                width: '10%',
                ...this.getColumnSearchProps('inter_fname'),
            },
            {
                title: 'Last Name',
                dataIndex: 'inter_lname',
                key: 'inter_lname',
                width: '10%',
                ...this.getColumnSearchProps('inter_lname'),
            },
            {
                title: 'Cemetery Section',
                dataIndex: 'cemetry_section',
                key: 'cemetry_section',
                ...this.getColumnSearchProps('cemetry_section'),
            },

            {
                title: 'Property Style',
                dataIndex: 'property_style',
                key: 'property_style',
                ...this.getColumnSearchProps('property_style'),
            },

            {
                title: 'Date of Death',
                dataIndex: 'date_of_death',
                key: 'date_of_death',
                ...this.getColumnSearchProps('date_of_death'),
            },
            {
                title: 'Interment Date',
                dataIndex: 'interment_date',
                key: 'interment_date',
                ...this.getColumnSearchProps('interment_date'),
            },
            {
                title: 'Action',
                key: 'action',
                render: (interred) => (
                    <span>
                        <a href={`/interred/${interred.id}`}> View Details </a>
                    </span>
                )
            }
        ];

        return (
          <div className="table-container">
              <div className="top">
                  <Button className="reset" onClick={this.clearAll} type="primary" >
                      Reset Filters
                  </Button>
                    {this.state.isFiltered ?
                            <div className="csv">
                                <CSVLink
                                filename={this.getFileName()}
                                className="csv"
                                onClick={() => {
                                    this.$CSVLink.current.link.download = this.getFileName();
                                }}
                                ref={this.$CSVLink}
                                data={this.state.filteredInfo.map(x => ({
                                  InterredId: x.id,
                                  Firstname: x.inter_fname,
                                  Lastname: x.inter_lname,
                                  Cemetry_section: x.cemetry_section,
                                  Property_style: x.property_style,
                                  Date_of_death: x.date_of_death,
                                  Interment_date: x.interment_date,
                                  Place_of_death: x.place_of_death,
                                  Mass_time: x.mass_time,
                                  Funeral_home_name: x.funeral_home_name,
                                  Church_name: x.church_name,
                                  Lot_number: x.lot_number,
                                  Grave_number: x.grave_number,
                                  Marital_status: x.marital_status,
                                  Veteran_status: x.veteran_status,
                                  Spouse_name: x.spouse_name,
                                  LotOwner: x.LotOwner,
                                  Gender: x.gender,
                                  Date_time: x.date_time,
                                  Age: x.age,
                                  Date_of_birth: x.date_of_birth,
                                  Birth_place: x.birth_place,
                                  Notes: x.notes,
                                  Created_by: this.createdBy(x.created_by_name),

                                }))}
                            >
                                    Download Filtered
                            </CSVLink>
                        </div>
                            :
                            <div className="csv">
                                <CSVLink
                                filename={this.getFileName()}
                                onClick={() => {
                                    this.$CSVLink.current.link.download = this.getFileName();
                                }}
                                ref={this.$CSVLink}
                                data={data.map(x => ({
                                  InterredId: x.id,
                                  Firstname: x.inter_fname,
                                  Lastname: x.inter_lname,
                                  Cemetry_section: x.cemetry_section,
                                  Property_style: x.property_style,
                                  Date_of_death: x.date_of_death,
                                  Interment_date: x.interment_date,
                                  Place_of_death: x.place_of_death,
                                  Mass_time: x.mass_time,
                                  Funeral_home_name: x.funeral_home_name,
                                  Church_name: x.church_name,
                                  Lot_number: x.lot_number,
                                  Grave_number: x.grave_number,
                                  Marital_status: x.marital_status,
                                  Veteran_status: x.veteran_status,
                                  Spouse_name: x.spouse_name,
                                  LotOwner: x.LotOwner,
                                  Gender: x.gender,
                                  Date_time: x.date_time,
                                  Age: x.age,
                                  Date_of_birth: x.date_of_birth,
                                  Birth_place: x.birth_place,
                                  Notes: x.notes,
                                  Created_by: this.createdBy(x.created_by_name),

                                }))}
                            >
                            Download
                    </CSVLink>
                </div>
            }
        </div>
                    <Table
                        key={this.state.index}
                        rowKey="id"
                        columns={columns}
                        onChange={this.handleChange}
                        dataSource={data}
                        pagination={{ pageSize: 12 }} />
                </div>

        )
    }
}
