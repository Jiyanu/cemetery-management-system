import React from "react";
import { Table, Input, Button,Tag } from 'antd';
import Highlighter from 'react-highlight-words';
import { CSVLink } from 'react-csv';

import './styles.scss'

export default class LotOwner extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            //data: this.props.data,
            searchText: '',
            searchedColumn: '',
            index: 0,
            filteredInfo: {},
            a: '',
            isFiltered: false,
        }
    };

    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                    Reset
                </Button>
            </div>
        ),

        filterIcon: filtered => <span className="search-icon" ><i className="fa fa-search" /></span>,
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text.toString()}
                />
            ) : (
                    text
                ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({
            searchText: '',
            isFiltered: false,
        });
    };

    clearAll = () => {
        this.setState({
            searchText: '',
            index: this.state.index + 1,
            isFiltered: false,
        })
    };

    handleChange = (pagination, filters, sorter, extra: { currentDataSource: [] }) => {
        let filteredData = extra.currentDataSource;
        this.setState({
            isFiltered: true,
            filteredInfo: filteredData,
        });
    }

    getCurrentDate(separator=''){

        let newDate = new Date()
        let date = newDate.getDate();
        let month = newDate.getMonth() + 1;
        let year = newDate.getFullYear();
        var s1 = '.csv'
        var s2 = `${year}${separator}${month<10?`0${month}`:`${month}`}${separator}${date}`
        return s2.concat(s1)
    }

    $CSVLink = React.createRef();

    getFileName() {
        let d = new Date();
        let dformat = `${d.getDate()}-${d.getMonth()+1}-${d.getFullYear()}`;
        return "lot-owner-" + dformat + ".csv";
    }

    render() {

        const { data } = this.props;
        console.log(data);
        const columns = [
            {
                title: 'Lot ID',
                dataIndex: 'id',
                key: 'id',
                width: '10%',
                defaultSortOrder: 'ascend',
                sorter: (a, b) => a.id - b.id,
                ...this.getColumnSearchProps('id'),
            },
            {
                title: 'First Name',
                dataIndex: 'FirstName',
                key: 'FirstName',
                width: '20%',
                ...this.getColumnSearchProps('FirstName'),
            },
            {
                title: 'Last Name',
                dataIndex: 'LastName',
                key: 'LastName',
                ...this.getColumnSearchProps('LastName'),
            },
            {
                title: 'Number of Lots',
                dataIndex: 'TotalNoOfLots',
                key: 'TotalNoOfLots',
                ...this.getColumnSearchProps('TotalNoOfLots'),
            },
            {
                title: 'Interred',
                dataIndex: 'interreds',
                key: 'interreds',
                render: interreds => (
                    <span>
                        {interreds.map(inter => {
                            return (
                                <i key={inter.id}>

                                    <a href={`/interred/${inter.id}`}> <Tag className="box" color="blue">{inter.inter_fname} {inter.inter_lname}</Tag></a>
                                </i>
                            );
                        })}
                    </span>
                ),

            },
            {
                title: 'Action',
                key: 'action',
                render: (lotOwner) => (
                    <span>
                        <a href={`/lot-owner/${lotOwner.id}`}> View Details </a>
                    </span>
                   ),
            },
        ];

        return (
            <div className="table-container">
                <div className="top">
                    <Button className="reset" onClick={this.clearAll} type="primary" >
                        Reset Filters
                    </Button>
                    { this.state.isFiltered ?
                            <div className="csv">
                                <CSVLink
                                filename={this.getFileName()}
                                className="csv"
                                onClick={() => {
                                    this.$CSVLink.current.link.download = this.getFileName();
                                }}
                                ref={this.$CSVLink}
                                data={this.state.filteredInfo.map(x => ({
                                    LotOwnerId: x.id,
                                    Firstname: x.FirstName,
                                    Lastname: x.LastName,
                                    StreetAddress: x.StreetAddress,
                                    AptNo: x.AptNo,
                                    City: x.City,
                                    State: x.State,
                                    Country: x.Country,
                                    Zip: x.Zip,
                                    Phone: x.PhoneNumber,
                                    DateIssued: x.DateIssued,
                                    Lots : x.TotalNoOfLots,
                                    Cost: x.Cost,
                                    Interred: x.interreds.map(y => {
                                        return (
                                        y.inter_fname + " " + y.inter_lname
                                        )
                                    })
                                }))}
                            >
                                    Download Filtered
                            </CSVLink>
                        </div>
                            :
                            <div className="csv">
                                <CSVLink
                                filename={this.getFileName()}
                                onClick={() => {
                                    this.$CSVLink.current.link.download = this.getFileName();
                                }}
                                ref={this.$CSVLink}
                                data={data.map(x => ({
                                    LotOwnerId: x.id,
                                    Firstname: x.FirstName,
                                    Lastname: x.LastName,
                                    StreetAddress: x.StreetAddress,
                                    AptNo: x.AptNo,
                                    City: x.City,
                                    State: x.State,
                                    Country: x.Country,
                                    Zip: x.Zip,
                                    Phone: x.PhoneNumber,
                                    DateIssued: x.DateIssued,
                                    Lots : x.TotalNoOfLots,
                                    Cost: x.Cost,
                                    Interred: x.interreds.map(y => {
                                        return (
                                        y.inter_fname + " " + y.inter_lname
                                        )
                                    })
                                }))}
                            >
                                    Download
                            </CSVLink>
                        </div>
                    }
                </div>
                <Table
                    key={this.state.index}
                    rowKey="id"
                    columns={columns}
                    dataSource={data}
                    onChange={this.handleChange}
                    pagination={{ pageSize: 12 }} />
            </div>
        )
    }
}
