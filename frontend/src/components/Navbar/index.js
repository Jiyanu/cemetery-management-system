import React from "react";
import { NavLink, withRouter, } from "react-router-dom";
import { connect } from 'react-redux';

import { Modal } from 'antd';

import './styles.scss'
import 'bootstrap/dist/css/bootstrap.min.css';

import * as actions from '../../../src/store/actions/auth.js';

class Navbar extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
        }
    }


    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = () => {
        this.props.logout()
        this.setState({
            visible: false,
        })
        setTimeout(() => {
            this.props.history.push('/')
        }, 1200);
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };


    render() {
        return (
            <div className="bar">
                <div className="header" to="/"> <span>Cemetery Management System</span></div>
                <div className="container">
                    {this.props.isAuthenticated ?
                        <React.Fragment>
                            <NavLink className="item" to="/lot-owner">Lot Owner</NavLink>
                            <NavLink className="item" to="/add-lot-owner">Add Lot Owner</NavLink>
                            <NavLink className="item" to="/interred">Interred</NavLink>
                            <NavLink className="item" to="/add-interred">Add Interred</NavLink>
                            <NavLink className="item" to="/add-tombstone">Add Tombstone</NavLink>
                            <span className="item" onClick={this.showModal}>Logout</span>
                        </React.Fragment>
                        :
                        <NavLink className="item" to="/">Login</NavLink>
                    }
                </div>

                <Modal
                    title="Logout"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <p>Are you sure you want to logout?</p>
                </Modal>
            </div>
        );
    }

}



const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(actions.logout())
    }
}

export default withRouter(connect(null, mapDispatchToProps)(Navbar));
