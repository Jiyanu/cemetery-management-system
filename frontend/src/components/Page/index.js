import React from 'react';

import Navbar from '../../components/Navbar';

import './styles.scss';

export default class Page extends React.Component {
    render() {

        return (
            <div className="layout">
                <Navbar {...this.props} />
                <div className="content">
                    {this.props.children}
                </div>
                <div className="footer">
                    Saints Peter & Paul Cemetery Management System @ 2020
                </div>
            </div>
        );
    }
}
