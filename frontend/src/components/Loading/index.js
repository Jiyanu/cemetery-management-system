import React from 'react';
import { Route } from 'react-router-dom';
import ReactLoading from 'react-loading';

import 'bootstrap/dist/css/bootstrap.css';

import NoURL from '../../../src/views/NoURL';
import './styles.scss';

export default class Loading extends React.PureComponent{
  

  constructor(props){
    super(props)
    this.state={
      done: undefined
    }
  }

  componentDidMount() {
    this.timer = setTimeout(() => {
      fetch('https://jsonplaceholder.typicode.com/posts')
      .then(response => response.json())
      .then(json => this.setState({ done : true}));
    }, 1200);
  }

  componentWillUnmount() {
    clearTimeout(this.timer)
  }

  render() {
    return(
      <div>
        {!this.state.done ? (
          <div className='load'>
            <ReactLoading className="ld" type={'spokes'} color={'white'} />
          </div>
        ) : (
            <Route component={NoURL}/>
          )}
      </div>
    );
  }
}

  //   _isMounted = false;

  //   state = {
  //     isLoading: true
  //   }
  
  //   componentDidMount() {
  //     this._isMounted = true;
    
  //       setTimeout(() => {
  //       fetch('https://jsonplaceholder.typicode.com/posts')
  //       .then(response => response.json())
  //       .then(result => {
  //           if (this._isMounted) {
  //               this.setState({isLoading: false})
  //           }
  //   }, 1200)
  //       }
  //     );
  //   }
  
    // componentWillUnmount() {
    //   this._isMounted = false;
    // }
  
  //   render() {
  //     return (
  //               <div>
  //               {!this.isLoading ? (
  //                   <div className='load'>
            //             <ReactLoading className="ld" type={'spokes'} color={'white'} />
            //         </div>
            //     ) : (
            //         <Route component={NoURL}/>
            //     )}
            // </div>
  //     );
  //   }
  // }