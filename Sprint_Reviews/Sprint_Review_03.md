## Prabesh:

- What went well?
    - Implementing the login/register authentication logic was tricky and interesting at the same time.
    - Creating an api request (GET) for lot_owner.

- What didn't go so well?
    - Implementing react-redux was the hard part.
    - Changing a dependency would break the build. So we had to make sure docker was up and running.
    - Fixing conflicts in git again took some extra time.

- What have I learned?
    - Implementing login/register authentication in react.
    - Creating an api using django rest framework.
    - Using the authentication logic to view the pages from Navbar only for logged in users.

- What still puzzles me?
    - The core idea behind react-redux and it's state management properties.
    - The redirection from login page on error puzzled me the most in this sprint.

- What will we change to improve?
    - We added dependencies for a small task. So, we'll try to reduce this to improve efficiency.
    - Now that we are moving to complex part, we should communicate with our client to get the proper modules.

## Gage:

- What went well?

    - Fiddling around with the basics was pretty easy.
    - Adding an image and including some styling worked very well.

- What didn't go so well?

    - I had problems adding the Navbar, despite it being easily added to the other components we had included.

- What have I learned?

    - Web Dev isn't my strongsuit, so working more with CSS styling and additional code has strengthened my ability to code accordingly.

- What still puzzles me?

    - The Navbar adding is eluding me. By the time this is read it will be solved, but it was just included in the other files that we've created. But with a 404 error page that includes all routes not explicitly added it isn't working right, so it's strange to me why it is so difficult to make work.

- What will we change to improve?

    - This Sprint we were finalizing some of the easier sections of code, so we didn't have much to improve in this current sprint, especially with our project breaking less this week than it has in previous weeks. However, I feel there is better structure we could include in our code to make it look better.

## Anusha:

- What went well?

    - Creation of model with different fields of the interred for backend went well. 
    - Git merge and commiting our individual stories to the develop branch went smooth. 
    - Everyone of us have given their own test cases of their issues in the TESTING.md this sprint.

- What didn't go so well?

    - Understanding serializers and working with sequence generation of unique id took time.

- What have I learned?

    - I learnt creating models using django and postgresql.

- What still puzzles me?

    - Adding relationships between the tables and figuring out how the lot_owner and interred tables communicate to get few fields automatically to the interred table from lot_owner.

- What will we change to improve?

    - We should go through requirements document and understand the next functionalities to work on
    - Summarize the to-do list for the next sprints by looking into the completed issues.

## Nberhe:

- What went well?
    
    - Creating the popup window was possible using reactjs popup library.
    - It could have been done easily using current existing library.   
    
- What didn't go so well?
    
    - Docker containers consume too much of the memory.
    - I wasnt able to compose and build the project for sometime.
    
- What have I learned?
    
    - Installing dependencies can slow down the progress of the team. 
    - Its better to keep the same design than to introduce a separate pattern.
    
- What still puzzles me?
    
    - How the retrived input is going to rerouted to the interred page.
    - Once its rerouted, how to dynamically create multiple section to add interred information according to the desired number of interred person inputed.
    
- What will we change to improve?
 
    - I will redesign the lot owner popup so it makes it easier for the backend react components to retrieve data in an organized way.
    - Do better research to design component with less dependencies.

    
    
    
