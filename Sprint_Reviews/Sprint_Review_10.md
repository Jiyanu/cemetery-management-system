## Prabesh:

- What went well?
    - Things are coming smoothly on the frontend side and we have finalized backend models.
    - Updated POST request of Tombstone, added new page for it.
    - Was able to fix the issue with empty nested object required fields.

- What didn't go so well?
    - Merge conflicts yet again. We have limited number of pages to work on. So everytime we have to deal with them.

- What have I learned?
    - Detailed inspeciton of REST API validations.
    - Continuous testing by chaning models in backend and connecting the dots in frontend.
    - Was able to add a new API to pull the users in the system.

- What still puzzles me?
    - Serverside POST request in deployment server still wont work.

- What will we change to improve?
    - We need to do more vigorous testing to find the bugs as we are approaching the last week.


## Gage:
- What went well?
    - Making the suitable changes for this sprint.

- What didn't go so well?
    - Miscommunication on what was expected for this sprint.

- What have I learned?
    - Even if it would bug someone, ask them anyway what the requirements are to make sure it's perfect.

- What still puzzles me?
    - Why people like React so much. Though I hear the others are somehow worse.

- What will we change to improve?
    - Look at the requirements from the client to make sure we have completed everything asked.



    
## Anusha:

- What went well?
    - Felt happy and satisfied this sprint
    - I could complete the issues that I stuck with in previous sprints like:
        - Post Request of Interred
        - Post Request of Tombstone with images
        - Put Request of Lot Owner with dynamic interreds

- What didn't go so well?
    - Got git merge issues, however Prabesh explained clearly how to avoid such mistakes again

- What have I learned?
    - Understood well to trace the network logs and to find where I am stuck
    - Performing post request with image and foreign key fields 
    - To override update method in serializer
    - Learnt from Prabesh how to pull foreign key fields as a drop down in a form

- What still puzzles me?
    - Whether to have delete operation for lot owner in frontend?

- What will we change to improve?
    - Test more, find bugs and fix issues 

## Nahom:

- What went well?
    - Modifying the interred in the lot owner table
    - Adding the download implementation for the interred table
    
- What didn't go so well?
    - Decided to implement the notification and validation for submission next sprint.

- What have I learned?
    - Was able to see the results of backend post requests and how it was implemented
    - Identified how user will be able to access the Django adminsteration interface

- What still puzzles me?
    - What method will be used to deliver the software.

- What will we change to improve?
    - Keep performing until finished product is available


