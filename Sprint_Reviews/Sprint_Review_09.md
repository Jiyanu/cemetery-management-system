## Prabesh:

- What went well?
    - Exporting to CSV from a json format from react went well.

- What didn't go so well?
    - I worked with Anusha to find the issue with the foreign key and encryption types. Those were really tricky.
    - We also ended up having some migration and db issues but were able to do a temp fix.

- What have I learned?
    - Exporting to CSV from a json object. I also learned to export based on filters applied.

- What still puzzles me?
    - multipat/format-data and application/json were not working properly. I had a hard time troubleshooting these issues.

- What will we change to improve?
    - Probably make the backend models simpler so we can keep track easily of what's going on.
    - Add a better validation mechanism on frontend to deubg REST API issues.
 

## Gage:

- What went well?
   - Modifying colums to work for the client's request.

- What didn't go so well?
    - I tested multiple more loads. Nothing ever works how I think it should.
    
- What have I learned?
  - I learned that solutions that work for someone else will rarely ever work for you.

- What still puzzles me?
   - Other than load, getting multiple events to occur on click.

- What will we change to improve?
    - Focus more on the conversation between the two ends.
    
## Anusha:

- What went well?
    - This sprint every issue is a puzzle as I have worked on the one which were not working before.
    - Put Request of Interred Details went well

- What didn't go so well?
    - Post request of Tombstone model when foreign key fields are present and image fields are present
    - Found difficulty with encryption types and uploading images
    - Took longer time than any of the sprint to deal with these issues

- What have I learned?
    - Post request and Put request in frontend
    - How to get created_by and LotOwner ids from other tables to frontend display
    - How to send details from one page to other

- What still puzzles me?
    - How to do post operation with image fields and foreign key fields
    - Why frontend gives not null integrity error eventhough if null=True in backend model?
    - Why it is difficult for post request when it involves image fields?

- What will we change to improve?
    - Everyone is new with the technologies, it's bit difficult to resolve the issues when no one is expertise in the technolgy that we are using. 
    - May be practice with already present opensource React and Django projects might help.

## Nahom:

- What went well?
    - Imported Select component from Antd
    - Drop down menu has a consistent display with the rest of the frontend

- What didn't go so well?
    - Client suggested improvement of column fields on tables
    
- What have I learned?
    - amazing report genertion with the react-csv implementated by Prabesh 

- What still puzzles me?
    - what kind of user docs to prepare for client walkthrough of the software
   
- What will we change to improve?
    - familarize myself with django rest api
