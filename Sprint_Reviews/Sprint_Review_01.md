## Prabesh:

- What went well?

  - The setup of docker including databse, frontend and server went pretty well.
  - With a single command now we can run all the things at once.

- What didn't go so well?

  - Setting up environment across three differnt OS didn't go well as expected.
  - We faced trouble fixing the dependencies while setting up the env.
  - Docker toolbox didn't work well on windows, so we had to switch to VM using ubuntu.

- What have I learned?

  - Creating multiple images in docker and setting up docker-compose.
  - Basic things about Django and React.

- What still puzzles me?

  - Setting up environment should have been easy across all OS, but again life's not that easy. This puzzled me and took a lot our our setup time to get things going well.

- What will we change to improve?
  - Now that all of us have it running well (at least on VMs). So we'll just go ahead with adding things with Django and React.

## Gage:

-- What went well?
  - Not much went well. Perhaps the best thing to happen so far is installing Visual Studio Code.

– What didn’t go so well?
  - Everything. I tried to install Docker using Windows Home, which is impossible without their toolbox. Their toolbox failed impressively and wouldn't load the frontend for us. Prabesh suggested getting dual-boot with Ubuntu...
  
  - That was awful. I eventually got it but I gave it 10GB because I consulted Google and underestimated how much space would be needed from responses given. I had to use GParted to give it more room, which runs the risk of bricking your computer!

  - Finally, back to Docker...I despise Docker. It took an amazingly long time to get it to even work in Ubuntu, but it is finally working. So yay?

– What have I learned?
  - I have learned when creating a new boot to give it more room than you think it will ever need to save yourself issues in the future. I have also learned how to properly merge branches as I tried to request a merge and did it incorrectly.

– What still puzzles me?
  - I don't know what still puzzles me because I got the development environment setup. However, I don't know how to work on a project of this caliber so I guess the ability to do so.

– What will we change to improve?
  - We haven't worked enough to know what to improve on, other than to just unload Windows from our computers.

## Anusha:

-What went well? 
  - Setting up the environment went well. This did not take more time.

– What didn’t go so well? 
  - Video uploading took very long time for me. As this is the first time, I took many trials to upload the correct one. Uploading more mb file is not possible in canvas. So compressed to 480p and then added to drive and shared the link.

– What have I learned?
  - I learnt using VS code to built django and react projects. I understood the concepts of docker-compose and how useful it is by reducing lot of time.

– What still puzzles me? 
  - My friends found issue while working in Windows. However, it was smooth while doing in Mac OS.

– What will we change to improve?
  - I shall reduce time for next-time while screen recording my work. I will take help of my team members to work more efficiently

## Nberhe:

-What went well? 
  - Installing docker on ubuntu was smooth compared to on windows.

– What didn’t go so well? 
  - Being a first time linux user, I faced some delay getting used to ubuntu and installing its dependencies.

– What have I learned?
  - I understand the dependendcies needed to run the project on docker required a a great amount of patience and research.  

– What still puzzles me? 
  - Why docker couldnt run the project built on react and django on windows?

– What will we change to improve?
  - I will work on improving my knowledge of docker so I can run projects efficiently depending on each systems running.