## Prabesh:

- What went well?
    - Deploying the whole project into production went well. We also managed to add search functionality which was the core client requirement.

- What didn't go so well?
    - Initially depolying everything on VM was daunting, but with some fixes it was succesfull.
    - I tried creating some environment variables for development and production but I was unable to do so.
    - We also came across client side form validation error which would fail our POST requests.

- What have I learned?
    - Depolying a react and django project in a server using caddy server.
    - Pulling data from the server and adding search functionality via react.

- What still puzzles me?
    - The environment variables and the caddy setup did not go smoothly as I wanted to. It worked for the time being but I want to revisit and make it better.

- What will we change to improve?
    - Add environment variables so we don't have to hard code the urls in server.
    - Add client side validation to make sure that POST requests are successful.
    - Manage login sessions between frontend and backend.

## Gage:

- What went well?
    - Proposed code changes went well and the issues for this week were completed.

- What didn't go so well?
    - Small issues with docker, but were easily fixed when my brain started working again.
    - Working with authentication isn't fun.

- What have I learned?
    - I learned more about authenticating and testing against that authentication.

- What still puzzles me?
    - Working with the 404 page and having it render along with another component didn't make sense to me as it wasn't an exact match like the components.

- What will we change to improve?
    - Work on the POST requests.

## Anusha:

- What went well?
    - Understanding the concepts behind Post request frontend and form functionalities with Axios
    - Being in a team and taking help with the team member to understand the errors in the code

- What didn't go so well?
    - There is a minor mistake in the code, that took very long time to trace the error with Post requests
    - Using Model, Client side validations did not go well while performing Post request
    - Date fields initially were a problem as the format in the frontend and backend was different

- What have I learned?
    - I learnt how to perform Post requests in frontend and applied it for Lot Owner individually
    - How backend and frontend fields have to be in sync, else that would result in error where the form doesn't accept it

- What still puzzles me?
    - Why the post request do not submit if the form is complex and it combines with other form and Model?

- What will we change to improve?
    - Synchronizing the backend and frontend data table fields, and finalizing on constraints will help to avoid getting the same errors as in this sprint

## Nahom:
- What went well?
   - Setting up the antd form

- What didn't go so well?
   - Looking for a way to add input options dynamically after a condition is met.
   - Composing and building the project. Additional dependencies challenged the build.
   
- What have I learned?
   - There would be an input required from the lot owner post request to redirect to the interred form and dynamically print the form according to the number of interred person recieved.

- What still puzzles me?
   - How would it import from the lot owner popup and print the interred form list.

- What will we change to improve?
   - Figure out the relationship between the post request anf interred form.
