## Prabesh:

- What went well?
    - Updating caddyfile and managing django-static files went well

- What didn't go so well?
    - I didn't make any changes to frontend side but for some reaon yarn test was failing and I couldn't figure out the solution.

- What have I learned?
    - Managing serverside session for frontend and backend

- What still puzzles me?
    - It was working perfectly fine for last commit, but suddenly test failed when I pushed changes that did not reflect frontend change at all.

- What will we change to improve?
    - Research more on yarn test to see what caused the error
    - Add environment variables so we don't have to hard code the urls in server.

## Gage:

- What went well?
    - We noticed an issue while pulling server-side where 404 page would flash and included a loading animation so it doesn't show.

- What didn't go so well?
    - Trying to push that same loading animation to other areas would work then crash, so that will be fixed for next sprint.

- What have I learned?
    - I have learned to not overthink the solution to problems. Working with the initial loading animation took much longer than it should have as I overthought it.

- What still puzzles me?
    - It astounds me how many different ways there are to create things using the React library but none seem to work the way I want them to.

- What will we change to improve?
    - From multiple sprints the code is bloated with unused code, so it should be cleaned up to prevent any future issues.

## Anusha:

- What went well?
    - Adding and removing dynamic fields for interred field in lot owner page went well

- What didn't go so well?
    - Git , I had many conflicts this time 
    - When trying to merge changes with the recent develop branch, stuck with issues 
    - Post request when I need select field to render foreign key values from another table

- What have I learned?
    - Learnt to render dynamic fields in frontend 
    - Trying to understand how to do post requests when foreign keys are involved

- What still puzzles me?
    - how interred records are added to database dynamically while doing from frontend

- What will we change to improve?
    - Being more cautious while pulling changes and adding my changes

## Nahom:
- What went well?
    - Functional event handler code for the select element on codepen.io

- What didn't go so well?
    - Memory issues, unable to locate react package warnings and unmigrated models on my side wouldnt let me build 
    - Integrating my part on the project would break things if not tested and built properly

- What have I learned?
    - Understanding react hooks and dynamic rendering.
     
- What still puzzles me?
    - Retrieving user input from the django models to be displayed on the interred page

- What will we change to improve?
    - Learn to link the django models to the interred page and give it a search functionality. 

