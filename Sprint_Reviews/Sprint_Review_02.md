## Prabesh:

- What went well?
  - Tracking issues and working on gitlab in individual branches was very smooth.
  - Writing models on django and managing django admin panel went well.


- What didn't go so well?
  - As we have few pages, it was difficult to delegate tasks among ourselves.
  - When we worked on same pages, we faced conflicts, it was time consuming.
  - Custom migration broke, so I had to roll-back to initial state and do the whole thing again.


- What have I learned?
  - Defining models in Django.
  - Writing custom migrations in Postgres in Django.
  - Managing git branches, merge requests and handling conflicts.

- What still puzzles me?
  - We are still not sure how to efficiently distribute tasks among ourselves as we are using new tech.

- What will we change to improve?
  - We will start working on modules so that our work would not overlap with each other and that way we would avoid conflicts.
  - Check with develop branch on how far our individual branch is, and keep it up to date.

## Gage:
- What went well?
    - Code was added and we progressed forward despite some early difficulties.
    
- What didn't go so well?
    - There have been a couple times just in this week that either frontend or backend have broken.
    - We had a merge conflict that was difficult to resolve due to learning to work with a group on a project of this magnitude.
    
- What have I learned?
    - While working with Git previously I am learning more how to use it with a larger group (still some issues).
	- I have learned how to use some React with some design frameworks. While I have watched videos our project is different and I learned how to manipulate accordingly.

- What still puzzles me?
	- I have some coding issues I need to work through, but Docker, and why we’re having so many issues, puzzles me greatly.

- What will we change to improve?
	- As stated previously we had a merge/coding conflict that we had to work through. While we thought they were two different issues we ran into troubles.
    - Through working more and more with Docker we will improve our skills with that and run into less issues.


## Anusha:
- What went well?
  - Creating a form with various input types like Select, Radio, Text, etc., in React went well. Also, the styling for the form is easy. It is like a brush up of javascript knowledge. Now, I feel React is not completely new, with few modifications, it is similar to Java web page designing w.r.t forms. 
  - Having worked on individual branches made merging of work easier
  
- What didn't go so well?
  - I had merge conflicts while working on my branch with the develop branch

- What have I learned?
  - React basics like components, form design, React-Router
  - Learnt to handle merge conflicts, 

- What still puzzles me?
  - How the backend works? I will take up backend issue for the next sprint and learn creating the models and work with Postgress database 

- What will we change to improve?
  - Our team has to sit and divide the tasks as soon the next sprint starts. This might be helpful to avoid conflicts.


## Nberhe:

- What went well?
  - Construction of the login page went well.
  
- What didn't go so well?
  - Redirecting to different sites of the page was challenging.
  - The process of importing the right libraries took extensive diagnosis.

- What have I learned?
  - Basics of react components and import libraries.
  - Dealing with forms and behaviour on react.

- What still puzzles me?
  - Looking forward to see how it all comes together to collect data.

- What will we change to improve?
  - Further analyzing the ways of Django and react components.
