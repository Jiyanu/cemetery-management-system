## Prabesh:

- What went well?
    - I restuctured the files and folders and made it consistent. This helped us avoid confusion on which file to edit.

- What didn't go so well?
    - I faced some issues with migration in test server deployment. So I could not fix it this time.

- What have I learned?
    - Refactoring code and adding search boxes in react.

- What still puzzles me?
    - I tried deploying, but there was one error after another. I'm close to finding solution but I guess I have to be patient.

- What will we change to improve?
    - Doing more vigorous testing and reporting bugs as issues
    - Do more research on deployment and fix that pesky issue.

## Gage:

- What went well?
    - Cleaning up code and adding a fix to the cost form where we now have 2 decimal places.

- What didn't go so well?
    - Loading is now added on logout but there is a memory leak in the code that needs fixed. Also included Regex for form validation that didn't get put in final code.

- What have I learned?
    - I learned that React is very fast and to be able to see something like a load you just need to add a delay for it to render.
    
- What still puzzles me?
    - At this point, the things that puzzle me are the things I don't know. Once I've solved a problem I feel much better, so I just need to solve more.

- What will we change to improve?
    - Logic, especially with post and get requests

## Anusha:

- What went well?
    - Being cautious with Git, made no merge conflicts this time 

- What didn't go so well?
    - Post request failed sevaral times. With reverse engineering, found that there are non-nullable objects in the model which I am missing in the form

- What have I learned?
    - I learnt how to post nested objects from an axios form  
    - Learn to retrieve values of a nested objects from a field
    - Overriding create method in serializers

- What still puzzles me?
    - Navigating from lot owner to interred page for editing details

- What will we change to improve?
    - Making a note of functionalities that are yet to be met would help to make clear of the tasks of every team member

## Nahom:
- What went well?
    - Finally was able to build the project on my side. 
    - Added additional route that could redirect user to store image files.

- What didn't go so well?
    - I was able to use hooks to redirect to the interred form page after storing image file.
    - React dom package should have enabled me but faced issues.

- What have I learned?
    - After any modal changes occur in the backend, one should remove previous postgress volumes.
    - This helped me build without any complication involving migration error
    
- What still puzzles me?
    - How we will able to map the interred burial lot. 
    - How we will be able store records in a pdf format and print invoices. 
    
- What will we change to improve?
    - Recieve more feedback from kim on the styling and functionality of the burial map.
