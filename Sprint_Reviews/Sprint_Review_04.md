## Prabesh:

- What went well?
    - Implementing gitlab CI for the react app was smooth. It was not easy as I had to run it multiple times to figure it out. I also figured out a way to deploy gitlab runner only when things are updated in develop branch.

- What didn't go so well?
    - We came across docker breakdown when Anusha updated database models, but our database had old values.
    - So, we had to delete our database as there were few columns that did not exist. This is not efficient in the long run but we need to find a solution to this.
    - I came across problem running python-django in gitlab runner. So I had to settle with react app for now.

- What have I learned?
    - Creating a gitlab-ci.yml file to implement gitlab runner.
    - Fixing docker when the database migration is altered.
    - Running yarn test on frontend components.

- What still puzzles me?
    - Running python-django and linking postgres database on Gitlab runner was not easy as expected. Our frontend ran smoothly but backend had some wierd dependency problems.

- What will we change to improve?
    - Research more on the gitlab CI and find proper configuration to run both frontend and backend smoothly.
    - Find an efficient way to handle the change in models in database so we don't have to reset the whole thing.

## Gage:

- What went well?
- What didn't go so well?
- What have I learned?
- What still puzzles me?
- What will we change to improve?

## Anusha:

- What went well?
    - Adding foreign key references to interred_info table went well
    - List of interred to display in backend is easy comparatively to the frontend.
    - Search for interred in URL in backend is easy to implement 
    - In Frontend, it took some time to understand and implement, but could get it. 

- What didn't go so well?
    - While adding fields in the existing models, while trying to view the list of interred, I faced various issues. Docker broke down, backend and frontend had issues. 
    - I have to completely remove the image of database 
    - I tried removing migration files and run it again. But nothing helped. 

- What have I learned?
    - I learnt adding rest request for backend and frontend. 
    - Learnt adding relations to the database tables 
    - Reseting the database helped to remove the database errors, which I get it after many hours of spending time on the same by trying various other ways

- What still puzzles me?
    - Wondering if there is an easy way to add fields in the tables

- What will we change to improve?
    - We have to think of some solution to avoid the present time-consuming way of adding fields. 

## Nahom:
- What went well?
    - Rewriting the form design and its attributes for the popup and compiling it on a web-based compiler.

- What didn't go so well?
    - Rebuilding docker because of insufficient memory caused by the storage consuming docker image files.

- What have I learned?
    - Removing all unused or dangling images, containers, volumes, and networks.
    - Reinstalling all packages needed for the docker build.
    - Grasped a general method on how values from the react forms will be redirected.

- What still puzzles me?
    - How to delete an entry once the user mistakenly submitted the data. A way to update the interred data from the lot owner page.

- What will we change to improve?
    - Implementing a step by step structured approach to build and run the react app to avoid time consumption.
