## Prabesh:

- What went well?
    - I was able to add PUT request for Lot Owners. Now we can update/edit from the frontend and reflect the changes in database.

- What didn't go so well?
    - For some reason GET was working and POST was not working in server-side deployment.
    - REST api validation is a bit confusing and had to debug a lot to figure out the issue.

- What have I learned?
    - Use of antd-design Select component to prefill the values.
    - Adding PUT requests to the backend.

- What still puzzles me?
    - Deployment is really troublesome to debug.
    - The Interred nested objects in backend is confusing.

- What will we change to improve?
    - Do more vigorous testing to figure out underlying issues.
    - Update each other constantly on where we are working so we can avoid merge conflicts.

## Gage:

- What went well?
    - With snippet of code from Prabesh, finishing the linking for lot owners and interred to edit their pages went well enough with a couple issues.

- What didn't go so well?
    - The loading animation had a memory leak and every time I fixed it something else would break.
    
- What have I learned?
    - I have learned to really look at the developer console for feedback on what is happening when using the website. Many fixes applied this week were due to that.

- What still puzzles me?
    - There is still an issue editing the interred after creation due to an error, despite the code being basically the same as the lot-owner.

- What will we change to improve?
    - Communication is always key, we became complacent after so long working on this.

## Anusha:

- What went well?
    - Adding a new model to our application with image fields

- What didn't go so well?
    - For adding new library Pillow for uploading images, it made me confused to add in the docker image

- What have I learned?
    - How we set path in settings file to store images uploaded from the model
    - Creating model with image fields and adding new libraries

- What still puzzles me?
    - Post request of interred with new tombstone model

- What will we change to improve?
    - We should make sure to list the issues left and plan on the left out ones for the next one month
    - Communicate more frequently on the current week issues by every individual

## Nahom:
- What went well?
    - Modal for popup worked similarly as the lotowner page. 

- What didn't go so well?
    - The select componenet wasn't fucntioning properly.
    - Compared to when it is outside the modal, select compnent had error inside the modal.

- What have I learned?
    - Correction for some errors wont be displayed clearly, it takes multiple debug method to figure out a way to solve the problem.

- What still puzzles me?
    - How our project is building fine on our own, but there are some issues on the bgsu host site. 

- What will we change to improve?
    - Notify early what issue I chose to be working on.